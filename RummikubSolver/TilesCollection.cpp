/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <assert.h>
#include <cmath>
#include "Tile.h"
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include "TilesCollection.h"
#include "TilesCollectionAssemblage.h"

TilesCollection::TilesCollection() : tileArray() {};

TilesCollection::TilesCollection(const TilesCollection &input) {
    tileArray = input.tileArray;
};

TilesCollection::TilesCollection(std::string input) {
    // We can read a TilesCollectionAssemblage2 format string here as well.
    // The individual groups and runs will then be lost
    boost::replace_all(input, "/", ",");

    boost::replace_all(input, "][", ",");
    boost::erase_all(input, "]");
    boost::erase_all(input, "[");

    std::vector<std::string> tempStrVec;
    // Split string into vector
    boost::algorithm::split(tempStrVec, input, boost::algorithm::is_any_of(","));
    // Loop through vector
    for (std::vector<std::string>::iterator it = tempStrVec.begin(); it != tempStrVec.end(); ++it) {
        // Should always be three chars XYY where X is color and YY is number
        assert((*it).size() == 3);
        // Check color
        Color tempColor;
        switch ((*it)[0]) {
            case '0':
                tempColor = NOCOLOR;
                break;
            case '1':
                tempColor = RED;
                break;
            case '2':
                tempColor = BLUE;
                break;
            case '3':
                tempColor = YELLOW;
                break;
            case '4':
                tempColor = BLACK;
                break;
            default:
                std::ostringstream ss;
                ss << "OMG WRONG COLOR: [" << (*it)[0] << "]\n";
                throw std::out_of_range(ss.str());
        }

        // Check number
        int tempNumber;
        tempNumber = std::stoi((*it).substr(1));
        if (tempNumber < 0 || tempNumber > 13) {
            std::ostringstream ss;
            ss << "OMG WRONG COLOR: [" << (*it)[0] << "]\n";
            throw std::out_of_range(ss.str());
        }
        Tile t(tempColor, tempNumber);
        tileArray.push_back(t);
    }
    std::sort(this->begin(), this->end());
}

void TilesCollection::add(Tile t) {
    tileArray.push_back(t);
    std::sort(this->begin(), this->end());
};

void TilesCollection::removeOne(Tile t) {
    tileIterator = std::find(tileArray.begin(), tileArray.end(), t);
    if (tileIterator != tileArray.end()) { // == vector.end() means the element was not found
        tileArray.erase(tileIterator);
    }
    std::sort(this->begin(), this->end());
}

void TilesCollection::removeAll(Tile t) {
    tileArray.erase(std::remove(tileArray.begin(), tileArray.end(), t), tileArray.end());
    std::sort(this->begin(), this->end());
}

/* Does not remove duplicates! */
void TilesCollection::removeTiles(const TilesCollection tc) {
    TilesCollection tcCopy(tc);
    std::vector<Tile>::iterator it = tileArray.begin();
    while (it != tileArray.end()) {
        if (tcCopy.containsTile(*it)) {
            tcCopy.removeOne(*it);
            it = tileArray.erase(it);
        } else ++it;
    }
    std::sort(this->begin(), this->end());
}

void TilesCollection::removeDuplicates() {
    sort(tileArray.begin(), tileArray.end());
    tileArray.erase(unique(tileArray.begin(), tileArray.end()), tileArray.end());
}


bool TilesCollection::containsColor(Color color) const {
    for (std::vector<Tile>::const_iterator it = tileArray.cbegin(); it != tileArray.cend(); ++it) {
        if ((*it).getColor() == color) {
            return true;
        }
    }
    return false;
};

bool TilesCollection::containsNumber(int number) const {
    for (std::vector<Tile>::const_iterator it = tileArray.cbegin(); it != tileArray.cend(); ++it) {
        if ((*it).getNumber() == number) {
            return true;
        }
    }
    return false;
};

bool TilesCollection::containsTile(Tile t) const {
    for (std::vector<Tile>::const_iterator it = tileArray.cbegin(); it != tileArray.cend(); ++it) {
        if ((*it) == t) {
            return true;
        }
    }
    return false;
}

int TilesCollection::countTile(Tile t) const {
    int count = 0;
    for (std::vector<Tile>::const_iterator it = tileArray.cbegin(); it != tileArray.cend(); ++it) {
        if ((*it) == t) {
            count++;
        }
    }
    return count;
}

bool TilesCollection::containsTiles(const TilesCollection tc) const {
    TilesCollection tcCopy(tc);
    for (std::vector<Tile>::const_iterator it = tileArray.cbegin(); it != tileArray.cend(); ++it) {
        if (tcCopy.containsTile(*it)) {
            //remove from tcCopy
            tcCopy.removeOne(*it);
        }
    }
    if (tcCopy.size() == 0) {
        return true;
    }
    return false;
}

TCType TilesCollection::getType() const {
    return NOTYPE;
}

bool TilesCollection::isLegal() const {
    return false;
}

bool TilesCollection::isVirginJoker() const {
    return tileArray.size() == 1 && tileArray[0].getNumber() == 0 && tileArray[0].getColor() == NOCOLOR &&
           tileArray[0].isJoker();
}

bool TilesCollection::canBeCastToRun() const{
    if (this->size() < 1) {
        std::cerr << "size of run is <1\n";
        return false;
    }

    if (this->size() > 13) {
        std::cerr << "size of run is >13\n";
        return false;
    }

    unsigned char joker_count = 0;
    unsigned char lastNumber = 0;
    Color c(NOCOLOR);
    for (Tile t: tileArray) {
        // Check that we have incrementing numbers
        //std::cout << "checking tile: " << t << "\n";
        if (t.isJoker()) {
            joker_count++;
            continue;
        }
        if (t.getNumber() > 0 && lastNumber != 0) {
            if (t.getNumber() != lastNumber + 1) {
                if (joker_count > 0) {
                    joker_count--;
                } else {
                    return false;
                }
            }
        }
        if (t.getNumber() > 0) {
            lastNumber = t.getNumber();
        }

        // Check that all tiles has the same color
        if (t.getColor() != c && c != NOCOLOR) {
            return false;
        }
        c = t.getColor();
    }
    return true;
}

bool TilesCollection::canBeCastToGroup() const {
    if (this->size() < 1) {
        //std::cerr << "size of group is <1\n";
        return false;
    }

    if (this->size() > 4) {
        //std::cerr << "size of group is >4\n";
        return false;
    }

    // Check that we do not have duplicates (skip jokers)
    bool colors[4];
    colors[0] = false;
    colors[1] = false;
    colors[2] = false;
    colors[3] = false;
    unsigned char lastNumber = 0;
    for (Tile t: tileArray) {
        // Check color
        unsigned char intColor = (unsigned char) t.getColor();
        if (intColor != 0) {
            if (colors[intColor - 1] == true) {
                //std::cerr << "group contains duplicate color \n";
                return false;
            } else {
                colors[intColor - 1] = true;
            }
        }

        // Check number
        if (t.getNumber() != 0 && lastNumber != 0) {
            if (t.getNumber() != lastNumber) {
                //std::cerr << "group contains duplicate number \n";
                return false;
            }
        }
        lastNumber = t.getNumber();
    }

    return true;
}

int TilesCollection::getSize() const {
    return tileArray.size();
};


Tile TilesCollection::at(std::vector<Tile>::size_type n) {
    return tileArray.at(n);
}

std::ostream &operator<<(std::ostream &os, const TilesCollection &tc) {
    for (std::vector<Tile>::const_iterator it = tc.cbegin(); it != tc.cend(); ++it) {
        // Normal output
        os << '[' << *it << ']';

        // Output suitable for input files
        //if (std::next(it) == tc.cend()) {
        //    os << *it;
        //} else {
        //    os << *it << ',';
        //}

    }
    return os;

}

bool TilesCollection::operator<(const TilesCollection &rhs) const {
    if (tileArray.size() != rhs.tileArray.size()) {
        return tileArray.size() < rhs.tileArray.size();
    } else {
        for (unsigned int i = 0; i < tileArray.size(); i++) {
            if (tileArray[i] != rhs.tileArray[i]) {
                return tileArray[i] < rhs.tileArray[i];
            }
        }
    }
    return false;
}

bool TilesCollection::operator==(const TilesCollection &other) const {
    if (tileArray.size() != other.tileArray.size()) {
        return false;
    } else {
        std::vector<Tile> thisCopy(this->tileArray);
        std::vector<Tile> otherCopy(other.tileArray);
        for (unsigned int i = 0; i < thisCopy.size(); i++) {
            if (thisCopy[i] != otherCopy[i]) {
                return false;
            }
        }
    }
    return true;
}

bool TilesCollection::operator!=(const TilesCollection &other) const {
    return !(*this).operator==(other);
}

std::vector<Tile, std::allocator<Tile> >::iterator TilesCollection::begin() {
    return tileArray.begin();
}

std::vector<Tile, std::allocator<Tile> >::iterator TilesCollection::end() {
    return tileArray.end();
}

std::vector<Tile, std::allocator<Tile> >::const_iterator TilesCollection::cbegin() const {
    return tileArray.cbegin();
}

std::vector<Tile, std::allocator<Tile> >::const_iterator TilesCollection::cend() const {
    return tileArray.cend();
}

std::vector<Tile, std::allocator<Tile> >::reverse_iterator TilesCollection::rbegin() {
    return tileArray.rbegin();
}

std::vector<Tile, std::allocator<Tile> >::reverse_iterator TilesCollection::rend() {
    return tileArray.rend();
}

std::vector<Tile, std::allocator<Tile> >::const_reverse_iterator TilesCollection::crbegin() const {
    return tileArray.crbegin();
}

std::vector<Tile, std::allocator<Tile> >::const_reverse_iterator TilesCollection::crend() const {
    return tileArray.crend();
}

void TilesCollection::reserve(std::vector<Tile>::size_type n) {
    tileArray.reserve(n);
}

std::vector<Tile>::size_type TilesCollection::size() const NOEXCEPT {
    return tileArray.size();
}

void TilesCollection::clear() NOEXCEPT {
    tileArray.clear();
}

bool TilesCollection::equals(TilesCollection &other) {
    return (tileArray == other.tileArray);
}

void TilesCollection::insert(std::vector<Tile, std::allocator<Tile> >::iterator position,
                             std::vector<Tile, std::allocator<Tile> >::iterator first,
                             std::vector<Tile, std::allocator<Tile> >::iterator last) {
    tileArray.insert(position, first, last);
}

TilesCollection TilesCollection::joinTilesCollections(TilesCollection a, TilesCollection b) {
    TilesCollection combinedTC;
    combinedTC.reserve(a.size() + b.size());
    combinedTC.insert(combinedTC.end(), a.begin(), a.end());
    combinedTC.insert(combinedTC.end(), b.begin(), b.end());

    std::sort(combinedTC.begin(),combinedTC.end());
    return combinedTC;
}

TilesCollectionAssemblage TilesCollection::getSubsets() const {
    TilesCollectionAssemblage result;
    result.reserve(pow(static_cast<double>(2), static_cast<double>(tileArray.size())));

    for (int i = 0; i < (1 << tileArray.size()); ++i) {
        TilesCollection tmp;
        for (unsigned int j = 0; j < tileArray.size(); ++j) {
            if (i & (1 << j)) {
                tmp.add(tileArray[j]);
            }
        }
        result.push_back(tmp);
    }
    return result;
}