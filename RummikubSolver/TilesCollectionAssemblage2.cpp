/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <vector>
#include <iostream>
#include <memory>
#include <boost/algorithm/string.hpp>
#include "Tile.h"
#include "TilesCollection.h"
#include "Run.h"
#include "Group.h"
#include "TilesCollectionAssemblage2.h"

TilesCollectionAssemblage2::TilesCollectionAssemblage2() : tcArray() {};

//this functions should only return Groups and Runs, and choke on other input
TilesCollectionAssemblage2::TilesCollectionAssemblage2(std::string input) {
    this->addSpecial(input);
}

TilesCollectionAssemblage2::TilesCollectionAssemblage2(const TilesCollectionAssemblage2 &input) {
    this->tcArray.reserve(input.tcArray.size());
    for (auto tmpTC : input.tcArray) {
        if (tmpTC->canBeCastToRun()) {
            Run tmpSpecial(*tmpTC);
            tcArray.push_back(std::make_shared<Run>(tmpSpecial));
        } else if (tmpTC->canBeCastToGroup()) {
            Group tmpSpecial(*tmpTC);
            tcArray.push_back(std::make_shared<Group>(tmpSpecial));
        } else {
            // Not a Run or a Group!!!
            std::cerr << "this is not a run or a group!: " << (*tmpTC) << "\n";
            assert(false);
        }
    }
};

void TilesCollectionAssemblage2::add(TilesCollection tc) {
    tcArray.push_back(std::make_shared<TilesCollection>(tc));
};

// This one adds TileCollections (no validation whatsoever)
void TilesCollectionAssemblage2::add(std::string input) {

    std::vector<std::string> tempStrVec;
    //Split string into vector
    boost::algorithm::split(tempStrVec, input, boost::algorithm::is_any_of("/"));
    for (std::vector<std::string>::iterator it = tempStrVec.begin(); it != tempStrVec.end(); ++it) {
        TilesCollection tmpTC((*it));
        tcArray.push_back(std::make_shared<TilesCollection>(tmpTC));
    }

}

// This one adds only valid Runs and Groups
void TilesCollectionAssemblage2::addSpecial(std::string input) {
    std::vector<std::string> tempStrVec;
    boost::algorithm::split(tempStrVec, input, boost::algorithm::is_any_of("/"));
    for (std::vector<std::string>::iterator it = tempStrVec.begin(); it != tempStrVec.end(); ++it) {
        TilesCollection tmpTC((*it));
        if (tmpTC.canBeCastToRun()) {
            Run tmpSpecial(tmpTC);
            tcArray.push_back(std::make_shared<Run>(tmpSpecial));
        } else if (tmpTC.canBeCastToGroup()) {
            Group tmpSpecial(tmpTC);
            tcArray.push_back(std::make_shared<Group>(tmpSpecial));
        } else {
            // Not a Run or a Group!!!
            std::cerr << "this is not a run or a group!: " << (*it) << "\n";
            assert(false);
        }
    }

}

void TilesCollectionAssemblage2::addTileToEach(Tile t) {
    for (auto it = tcArray.begin(); it != tcArray.end(); ++it) {
        (*it)->add(t);
    }
}

void TilesCollectionAssemblage2::addTileToEven(Tile t) {
    for (unsigned int i = 0; i < tcArray.size(); i++) {
        if (i % 2 == 0) {
            tcArray[i]->add(t);
        }
    }
}

void TilesCollectionAssemblage2::addTileToOdd(Tile t) {
    for (unsigned int i = 0; i < tcArray.size(); i++) {
        if (i % 2 == 1) {
            tcArray[i]->add(t);
        }
    }
}

void TilesCollectionAssemblage2::removeOne(TilesCollection tc) {
    for (auto it = tcArray.begin(); it != tcArray.end();) {
        if ((**it) == tc) {
            it = tcArray.erase(it);
            // Only erase one
            return;
        } else {
            ++it;
        }
    }
}

/**
 * @return amount of TileCollections in object
 */
unsigned int TilesCollectionAssemblage2::size() const NOEXCEPT {
    return tcArray.size();
}

/**
 * @return amount of Tiles in object
 */
unsigned int TilesCollectionAssemblage2::sizeTotal() const NOEXCEPT {
    unsigned int result = 0;
    for (auto tc:tcArray) {
        result += tc->size();
    }
    return result;
}

bool TilesCollectionAssemblage2::containsTile(Tile t) const {
    for (auto it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if ((*it)->containsTile(t)) {
            return true;
        }
    }
    return false;
}

unsigned int TilesCollectionAssemblage2::countTile(Tile t) const {
    unsigned int count = 0;
    for (auto it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if ((*it)->containsTile(t)) {
            count++;
        }
    }
    return count;
}

unsigned int TilesCollectionAssemblage2::countTilesCollection(TilesCollection tc) const {
    unsigned int count = 0;
    for (auto it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if ((**it) == tc) {
            count++;
        }
    }
    return count;
}

TilesCollectionAssemblage2 TilesCollectionAssemblage2::getDuplicates() const {
    TilesCollectionAssemblage2 result;
    for (auto it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if (this->countTilesCollection(**it) > 1 && !result.countTilesCollection(**it)) {
            result.add(**it);
        }
    }
    return result;
}

void TilesCollectionAssemblage2::sort() {
    // Sort every TilesCollection
    for (auto it = tcArray.begin(); it != tcArray.end(); ++it) {
        std::sort((*it)->begin(), (*it)->end());
    }
    // Needs custom sorting so we are not sorting by pointers
    struct {
        bool operator()(std::shared_ptr<TilesCollection> &a, std::shared_ptr<TilesCollection> &b) const
        {
            return *a < *b;
        }
    } customLess;
    std::sort(tcArray.begin(), tcArray.end(), customLess);
}

void TilesCollectionAssemblage2::duplicate() {
    if (tcArray.size() > 0) {
        std::vector<tcPointer> tcArrayCopy = tcArray;
        tcArray.reserve(tcArray.size() + 1);
        for (auto it = tcArrayCopy.begin(); it != tcArrayCopy.end(); ++it) {
            tcArray.push_back((*it));
        }
    }
}

bool TilesCollectionAssemblage2::equals(TilesCollectionAssemblage2 &other) {
    (*this).sort();
    other.sort();
    return (tcArray == other.tcArray);
}

std::ostream &operator<<(std::ostream &os, const TilesCollectionAssemblage2 &tc) {
    for (auto it = tc.cbegin(); it != tc.cend(); ++it) {
        // Normal output
        //os << **it << "\n";
        // Output whether tilecollection is a run or a group or both
        //os << **it << " run: " << (*it)->canBeCastToRun() << " group: " << (*it)->canBeCastToGroup() << "\n";
        // Print on one line
        os << **it << "/";
    }
    return os;
}

bool TilesCollectionAssemblage2::operator<(const TilesCollectionAssemblage2 &rhs) const {
    if (this->size() == rhs.size()) {
        return this->sizeTotal() < rhs.sizeTotal();
    } else {
        return this->size() < rhs.size();
    }
}

bool TilesCollectionAssemblage2::operator==(const TilesCollectionAssemblage2 &other) const {
    if (tcArray.size() != other.tcArray.size()) {
        return false;
    } else {
        std::vector<tcPointer> thisCopy(this->tcArray);
        std::vector<tcPointer> otherCopy(other.tcArray);

        std::sort(thisCopy.begin(), thisCopy.end());
        std::sort(otherCopy.begin(), otherCopy.end());

        for (unsigned int i = 0; i < thisCopy.size(); i++) {
            if ((*thisCopy[i]) != (*otherCopy[i])) {
                return false;
            }
        }
    }
    return true;
}

bool TilesCollectionAssemblage2::operator!=(const TilesCollectionAssemblage2 &other) const {
    return !((*this) == other);
}

TilesCollection& TilesCollectionAssemblage2::at(std::vector<TilesCollection>::size_type n) {
    return *tcArray.at(n);
}

/**
 * This is an ugly hack to avoid object typeslicing
 * @param n
 * @param val
 */
void TilesCollectionAssemblage2::insertAtGroup(std::vector<TilesCollection>::size_type n, std::shared_ptr<Group>&val) {
    tcArray.at(n) = val;
}

/**
 * This is an ugly hack to avoid object typeslicing
 * @param n
 * @param val
 */
void TilesCollectionAssemblage2::insertAtRun(std::vector<TilesCollection>::size_type n, Run val) {
    tcArray.at(n) = std::make_shared<Run>(val);
}

std::vector<tcPointer, std::allocator<tcPointer> >::iterator TilesCollectionAssemblage2::begin() {
    return tcArray.begin();
}

std::vector<tcPointer, std::allocator<tcPointer> >::iterator TilesCollectionAssemblage2::end() {
    return tcArray.end();
}

std::vector<tcPointer, std::allocator<tcPointer> >::const_iterator TilesCollectionAssemblage2::cbegin() const {
    return tcArray.cbegin();
}

std::vector<tcPointer, std::allocator<tcPointer> >::const_iterator TilesCollectionAssemblage2::cend() const {
    return tcArray.cend();
}

void TilesCollectionAssemblage2::push_back(const TilesCollection &val) {
    tcArray.push_back(std::make_shared<TilesCollection>(val));
}

void TilesCollectionAssemblage2::push_back(TilesCollection &&val) {
    tcArray.push_back(std::make_shared<TilesCollection>(val));
}

void TilesCollectionAssemblage2::push_back(Run &&val) {
    tcArray.push_back(std::make_shared<Run>(val));
}

void TilesCollectionAssemblage2::push_back(Group &&val) {
    tcArray.push_back(std::make_shared<Group>(val));
}

void TilesCollectionAssemblage2::push_back(const Group &val) {
    tcArray.push_back(std::make_shared<Group>(val));
}

void TilesCollectionAssemblage2::push_back(std::shared_ptr<Group>&val) {
    tcArray.push_back(val);
}

void TilesCollectionAssemblage2::reserve(std::vector<TilesCollection>::size_type n) {
    tcArray.reserve(n);
}
