/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TILESCOLLECTION_H
#define TILESCOLLECTION_H
#include "crossplatform.h"
#include "Tile.h"

class TilesCollectionAssemblage;

enum TCType {
    NOTYPE = 0, RUN = 1, GROUP = 2,
};

class TilesCollection {
public:
    TilesCollection();
    TilesCollection(const TilesCollection &input);
    TilesCollection(std::string input);
    void add(Tile t);
    void removeOne(Tile t);
    void removeAll(Tile t);
    void removeTiles(TilesCollection tc);
    void removeDuplicates();
    int getSize() const;
    Tile at(std::vector<Tile>::size_type n);
    bool containsColor(Color color) const;
    bool containsNumber(int number) const;
    bool containsTile(const Tile t) const;
    int countTile(Tile t) const;
    bool containsTiles(const TilesCollection tc) const;
    virtual TCType getType() const;
    virtual bool isLegal() const;
    bool isVirginJoker() const;
    bool canBeCastToRun() const;
    bool canBeCastToGroup() const;
    friend std::ostream &operator<<(std::ostream &os, const TilesCollection &tc);
    bool operator<(const TilesCollection &rhs) const;
    bool operator==(const TilesCollection &other) const;
    bool operator!=(const TilesCollection &other) const;
    std::vector<Tile, std::allocator<Tile> >::iterator begin();
    std::vector<Tile, std::allocator<Tile> >::iterator end();
    std::vector<Tile, std::allocator<Tile> >::const_iterator cbegin() const;
    std::vector<Tile, std::allocator<Tile> >::const_iterator cend() const;
    std::vector<Tile, std::allocator<Tile> >::reverse_iterator rbegin();
    std::vector<Tile, std::allocator<Tile> >::reverse_iterator rend();
    std::vector<Tile, std::allocator<Tile> >::const_reverse_iterator crbegin() const;
    std::vector<Tile, std::allocator<Tile> >::const_reverse_iterator crend() const;
    void reserve(std::vector<Tile>::size_type n);
    std::vector<Tile>::size_type size() const NOEXCEPT;
    void clear() NOEXCEPT;
    bool equals(TilesCollection &other);
    void insert(std::vector<Tile, std::allocator<Tile> >::iterator position,
                std::vector<Tile, std::allocator<Tile> >::iterator start,
                std::vector<Tile, std::allocator<Tile> >::iterator end);
    static TilesCollection joinTilesCollections(TilesCollection a, TilesCollection b);
    TilesCollectionAssemblage getSubsets() const;

protected:
    std::vector<Tile> tileArray;
    std::vector<Tile>::iterator tileIterator;
    std::vector<Tile>::const_iterator constTileIterator;
    friend class Run;
    friend class Group;
};

#endif
