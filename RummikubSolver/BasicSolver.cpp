/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <sstream>
#include <algorithm>
#include <vector>
#include <memory>
#include <iostream>
#include "TilesCollection.h"
#include "TilesCollectionAssemblage.h"
#include "BasicSolver.h"

TilesCollectionAssemblage BasicSolver::Solve(TilesCollection table, TilesCollection rack) {
    TilesCollectionAssemblage rackSubsets;
    TilesCollectionAssemblage result;


    //Try first with whole rack,
    //then remove one tile etc until we are testing with one tile each
    rackSubsets = rack.getSubsets();

    //std::cout << "Rack subset count: " << rackSubsets.size() << "\n";

    //sort rackSubsets according to size, largest first
    std::sort(rackSubsets.begin(), rackSubsets.end(),
              [](const TilesCollection &a, const TilesCollection &b) {
                  return a.size() > b.size();
              });

    for (auto it = rackSubsets.begin(); it != rackSubsets.end(); ++it) {
        std::cout << "Trying rack: " << *it << "\n";
        TilesCollectionAssemblage possibleCombinations;
        TilesCollection combinedTC;
        combinedTC = TilesCollection::joinTilesCollections(table, (*it));
        possibleCombinations = getPossibleCombinations(combinedTC);

        //std::cout << "possibleCombinations:\n" << possibleCombinations << "\n";
        //std::cout << "Possible combinations count: " << possibleCombinations.size() << "\n";

        result = findSolution(possibleCombinations, combinedTC);
        if (result.size() > 0) {
            //std::cout << "Found solution! Used " << (*it).size() << "/" << rack.size() << " tiles from rack\n";
            return result;
        }
    }

    return result;
}

TilesCollectionAssemblage
BasicSolver::findSolution(TilesCollectionAssemblage possibleCombinations, TilesCollection allTiles) {
    // Loop over all possible combinations
    for (std::vector<TilesCollection>::iterator it = possibleCombinations.begin();
         it != possibleCombinations.end(); it++) {
        // Create a copy of allTiles (this is necessary)
        TilesCollection copyAllTiles(allTiles);

        // Check if current combination exists in copyAllTiles
        if (copyAllTiles.containsTiles(*it)) {
            // Remove current combination from copyAllTiles
            copyAllTiles.removeTiles(*it);
        } else {
            // Goto next combination
            continue;
        }

        //If copyAllTiles is now empty, we have a solution -> return current combination
        if (copyAllTiles.size() == 0) {
            TilesCollectionAssemblage result;
            result.push_back(*it);
            return result;
        }

        TilesCollectionAssemblage result;
        result = findSolution(possibleCombinations, copyAllTiles);

        //If result is non-empty, we have found a solution! -> add current combination to result and return.
        if (result.size() > 0) {
            result.push_back(*it);
            return result;
        }
    }

    // No solution found, return empty
    TilesCollectionAssemblage result;
    return result;
}

//This gets every possible combination we can do with the input tiles
//"Dumb" algorithm
TilesCollectionAssemblage BasicSolver::getPossibleCombinations(TilesCollection combinedTC) {
    //TODO stop using stupid hacks
    TilesCollectionAssemblage totalPossibleCombinations(

#include "1773TilesCollections.inc"

    );
    TilesCollectionAssemblage result;

    for (std::vector<TilesCollection>::iterator tcit = totalPossibleCombinations.begin();
         tcit != totalPossibleCombinations.end(); ++tcit) {
        if (combinedTC.containsTiles(*tcit)) {
            result.add(*tcit);
        }
    }

    return result;
}

TilesCollectionAssemblage BasicSolver::getPossibleCombinations(TilesCollection table, TilesCollection rack) {
    TilesCollection combinedTC = TilesCollection::joinTilesCollections(table, rack);

    return getPossibleCombinations(combinedTC);

}

