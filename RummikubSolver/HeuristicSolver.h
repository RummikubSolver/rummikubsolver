/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef HEURISTICSOLVER_H
#define HEURISTICSOLVER_H

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/heap/priority_queue.hpp>

#include "Solver.h"
#include "Node.h"

struct LogicalState;
class HeuristicSolver;
typedef std::shared_ptr<Node> NodePointer;

class CompareNodePointers {
public:
    bool operator()(const NodePointer &np1, const NodePointer &np2) const {
        return (*np1) > (*np2);
    }
};


class HeuristicSolver : public Solver {
public:
    HeuristicSolver();
    TilesCollectionAssemblage2 Solve(TilesCollectionAssemblage2 table, TilesCollection rack);
    TilesCollectionAssemblage2 Solve(TilesCollectionAssemblage2 table, TilesCollection rack, std::size_t timeout);
    TilesCollection getHand();
    bool isInMap(NodePointer np);
    NodePointer getFromMap(NodePointer np);
    void insertToMap(NodePointer np);
    unsigned int getCardCount();
    void setCardCount(unsigned int value);
    static std::size_t hash_value(Node const &n);

protected:
    LogicalState Search(int maxMS);
    NodePointer processNode(NodePointer node);
    boost::heap::priority_queue<NodePointer, boost::heap::compare<CompareNodePointers>> fringe;
    // The nodes that we have already calculated
    boost::unordered_set<std::size_t> closed;
    boost::unordered_map<Node, NodePointer> idMap;
    NodePointer root;
    TilesCollection hand;
    unsigned int cardCount;
};


#endif
