/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BASICSOLVER_H
#define BASICSOLVER_H

#include "Solver.h"

class BasicSolver : public Solver {
public:
    virtual TilesCollectionAssemblage Solve(TilesCollection table, TilesCollection rack);
    virtual TilesCollectionAssemblage getPossibleCombinations(TilesCollection table, TilesCollection rack);
    TilesCollectionAssemblage getPossibleCombinations(TilesCollection combinedTC);
    TilesCollectionAssemblage findSolution(TilesCollectionAssemblage possibleCombinations, TilesCollection allTiles);

protected:
    TilesCollectionAssemblage possibleCombinations;
};


#endif
