/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <sstream>
#include <algorithm>
#include <vector>
#include <memory>
#include <iostream>
#include <chrono>
#include "Tile.h"
#include "TilesCollection.h"
#include "TilesCollectionAssemblage2.h"
#include "Node.h"
#include "Solver.h"
#include "HeuristicSolver.h"


HeuristicSolver::HeuristicSolver() {
}

TilesCollectionAssemblage2 HeuristicSolver::Solve(TilesCollectionAssemblage2 table, TilesCollection rack) {
    return Solve(table, rack, 5000);
}

TilesCollectionAssemblage2 HeuristicSolver::Solve(TilesCollectionAssemblage2 table, TilesCollection rack, std::size_t timeout) {
    this->cardCount = rack.size();
    for (auto tc:table)
        this->cardCount += tc->size();

    this->hand = rack;
    TilesCollectionAssemblage2 allCards(table);
    for (std::vector<Tile>::iterator it = rack.begin(); it != rack.end(); ++it) {
        TilesCollection tempTC;
        tempTC.add(*it);
        allCards.add(tempTC);
    }
    root = std::make_shared<Node>(nullptr, allCards, 0, this);

    fringe.push(root);
    idMap.insert(std::make_pair(*root, root));

    // Do the actual search
    LogicalState result = this->Search(timeout);

    return result.board;
}

LogicalState HeuristicSolver::Search(int maxMS) {
    std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();

    NodePointer result;

    while (!fringe.empty() && (!maxMS ||
           (std::chrono::duration_cast<std::chrono::milliseconds>(
                   std::chrono::high_resolution_clock::now() - startTime
           ).count()) < maxMS)) {
        //std::cout << "Processing from fringe:\n............" << fringe.top() << "............\n";
        NodePointer resultTemp = processNode(fringe.top());

        /*
        // Debug
        if (resultTemp != nullptr){
            std::cout << "Found possible solution:\n-----" << resultTemp << "-----\n";
        }
        */

        if (resultTemp != nullptr && (result == nullptr || (*resultTemp) <
                                                           (*result))) {
            result = resultTemp;
            // Debug
            //std::cout << "Updated best right now:" << resultTemp << "-----\n";
            if (resultTemp->getState().hand.size() == 0) {
                break;
            }
        }
    }

    std::cout << "Ended Search() because ";
    if (fringe.empty()) {
        std::cout << "fringe is empty\n";
    } else if (result->getState().hand.size() == 0){
        std::cout << "hand is empty\n";
    } else {
        std::cout << "time-out reached\n";
    }
    if (result == nullptr) {
        std::cout << "Search() returning empty logicalstate\n";
        return LogicalState();
    }
    return (*result).getState();
}

NodePointer HeuristicSolver::processNode(NodePointer node) {
    fringe.pop();
    // Insert this node hash into "already processed" set
    closed.insert(Node::hash_value(*node));
    //std::cout << "------------------\n";
    //std::cout << "Hash: " << Node::hash_value(*node) << "\n";
    //std::cout << *node << "\n";
    //std::cout << "------------------\n";
    std::vector<NodePointer> neighbours = node->getNeighbors();
    //std::cout << "processNode() neighbours.size() " << neighbours.size() << "\n";

    for (auto it = closed.begin(); it != closed.end(); ++it) {
        neighbours.erase(std::remove_if(neighbours.begin(), neighbours.end(), [it](NodePointer np) {
            if ((*it) == Node::hash_value(*np)) {/*std::cout << "\nerasing \n|||" << *np << "|||\n";*/ return true; }
            { /*std::cout << "\nNOT erasing \n|||" << *np << "|||\n";*/ return false; }
        }), neighbours.end());

    }
    for (auto it = neighbours.begin(); it != neighbours.end(); ++it) {
        fringe.push(*it);
    }

    // Checks that all tiles from table are now in legal collections
    if (node->isValid()) {
        //std::cout << "Node was valid!\n";
        return node;
    }
    //std::cout << "Node was NOT valid!\n";
    return nullptr;
}

TilesCollection HeuristicSolver::getHand() {
    return hand;
}

bool HeuristicSolver::isInMap(NodePointer np) {
    return idMap.count(*np) > 0;
}

void HeuristicSolver::insertToMap(NodePointer np) {
    idMap.insert(std::make_pair(*np, np));
}

unsigned int HeuristicSolver::getCardCount() {
    return cardCount;
}

void HeuristicSolver::setCardCount(unsigned int value) {
    cardCount = value;
}

std::size_t hash_value(Node const &n) {
    return Node::hash_value(n);
}

NodePointer HeuristicSolver::getFromMap(NodePointer np) {
    return idMap.at(*np);
}
