/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TILESCOLLECTIONASSEMBLAGE2_H
#define TILESCOLLECTIONASSEMBLAGE2_H
#include "TilesCollection.h"
#include "Run.h"
#include "Group.h"

typedef std::shared_ptr<TilesCollection> tcPointer;

class TilesCollectionAssemblage2 {
public:
    TilesCollectionAssemblage2();
    TilesCollectionAssemblage2(std::string input);
    TilesCollectionAssemblage2(const TilesCollectionAssemblage2 &input);
    void add(TilesCollection tc);
    void add(std::string input);
    void addSpecial(std::string input);
    void addTileToEach(Tile t);
    void addTileToEven(Tile t);
    void addTileToOdd(Tile t);
    void removeOne(TilesCollection tc);
    unsigned int size() const NOEXCEPT;
    unsigned int sizeTotal() const NOEXCEPT;
    bool containsTile(Tile t) const;
    unsigned int countTile(Tile t) const;
    unsigned int countTilesCollection(TilesCollection tc) const;
    TilesCollectionAssemblage2 getDuplicates() const;
    void sort();
    void duplicate();
    bool equals(TilesCollectionAssemblage2 &other);
    friend std::ostream &operator<<(std::ostream &os, const TilesCollectionAssemblage2 &tc);
    bool operator<(const TilesCollectionAssemblage2 &rhs) const;
    bool operator==(const TilesCollectionAssemblage2 &other) const;
    bool operator!=(const TilesCollectionAssemblage2 &other) const;
    TilesCollection &at(std::vector<TilesCollection>::size_type n);
    void insertAtGroup(std::vector<TilesCollection>::size_type n, std::shared_ptr<Group>&val);
    void insertAtRun(std::vector<TilesCollection>::size_type n, Run val);
    std::vector<tcPointer, std::allocator<tcPointer> >::iterator begin();
    std::vector<tcPointer, std::allocator<tcPointer> >::iterator end();
    std::vector<tcPointer, std::allocator<tcPointer> >::const_iterator cbegin() const;
    std::vector<tcPointer, std::allocator<tcPointer> >::const_iterator cend() const;
    void push_back(const TilesCollection &val);
    void push_back(TilesCollection &&val);
    void push_back(Run &&val);
    void push_back(Group &&val);
    void push_back(const Group &val);
    void push_back(std::shared_ptr<Group>&val);
    void reserve(std::vector<TilesCollection>::size_type n);
protected:
    std::vector<tcPointer> tcArray;
};

#endif
