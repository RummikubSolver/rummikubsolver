# Compiler flags...
TEST__CPP_COMPILER = g++
TEST__C_COMPILER = gcc


# Library paths...
Test__Debug_Library_Path=
Test__Release_Library_Path=

# Additional libraries...
Test__Debug_Libraries=-Wl,--start-group -pthread -lgtest  -Wl,--end-group
Test__Release_Libraries=-Wl,--start-group -pthread -lgtest  -Wl,--end-group

# Preprocessor definitions...
Test__Debug_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE 
Test__Release_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE 

# Implictly linked object files...
Test__Debug_Implicitly_Linked_Objects=
Test__Release_Implicitly_Linked_Objects=

# Compiler flags...
Test__Debug_Compiler_Flags=-Wall -O0 -g -std=c++11 
Test__Release_Compiler_Flags=-Wall -O2 -std=c++11

# Builds all configurations for this project...
.PHONY: test__build_all_configurations
test__build_all_configurations: Test__Debug Test__Release

# Builds the Debug configuration...
.PHONY: Test__Debug
Test__Debug: test__create_folders test__gccDebug/RummikubSolverUnitTest.o test__gccDebug/stdafx.o 
	$(TEST__CPP_COMPILER) test__gccDebug/RummikubSolverUnitTest.o test__gccDebug/stdafx.o  $(Test__Debug_Library_Path) $(Test__Debug_Libraries) -Wl,-rpath,./ -o ../test__gccDebug/RummikubSolverUnitTest.exe

# Compiles file RummikubSolverUnitTest.cpp for the Debug configuration...
-include test__gccDebug/RummikubSolverUnitTest.d
test__gccDebug/RummikubSolverUnitTest.o: RummikubSolverUnitTest.cpp
	$(TEST__CPP_COMPILER) $(Test__Debug_Preprocessor_Definitions) $(Test__Debug_Compiler_Flags) -c RummikubSolverUnitTest.cpp $(Test__Debug_Include_Path) -o test__gccDebug/RummikubSolverUnitTest.o
	$(TEST__CPP_COMPILER) $(Test__Debug_Preprocessor_Definitions) $(Test__Debug_Compiler_Flags) -MM RummikubSolverUnitTest.cpp $(Test__Debug_Include_Path) > test__gccDebug/RummikubSolverUnitTest.d

# Compiles file stdafx.cpp for the Debug configuration...
-include test__gccDebug/stdafx.d
test__gccDebug/stdafx.o: stdafx.cpp
	$(TEST__CPP_COMPILER) $(Test__Debug_Preprocessor_Definitions) $(Test__Debug_Compiler_Flags) -c stdafx.cpp $(Test__Debug_Include_Path) -o test__gccDebug/stdafx.o
	$(TEST__CPP_COMPILER) $(Test__Debug_Preprocessor_Definitions) $(Test__Debug_Compiler_Flags) -MM stdafx.cpp $(Test__Debug_Include_Path) > test__gccDebug/stdafx.d

# Builds the Release configuration...
.PHONY: Test__Release
Test__Release: test__create_folders test__gccRelease/RummikubSolverUnitTest.o test__gccRelease/stdafx.o 
	$(TEST__CPP_COMPILER) test__gccRelease/RummikubSolverUnitTest.o test__gccRelease/stdafx.o  $(Test__Release_Library_Path) $(Test__Release_Libraries) -Wl,-rpath,./ -o ../test__gccRelease/RummikubSolverUnitTest.exe

# Compiles file RummikubSolverUnitTest.cpp for the Release configuration...
-include test__gccRelease/RummikubSolverUnitTest.d
test__gccRelease/RummikubSolverUnitTest.o: RummikubSolverUnitTest.cpp
	$(TEST__CPP_COMPILER) $(Test__Release_Preprocessor_Definitions) $(Test__Release_Compiler_Flags) -c RummikubSolverUnitTest.cpp $(Test__Release_Include_Path) -o test__gccRelease/RummikubSolverUnitTest.o
	$(TEST__CPP_COMPILER) $(Test__Release_Preprocessor_Definitions) $(Test__Release_Compiler_Flags) -MM RummikubSolverUnitTest.cpp $(Test__Release_Include_Path) > test__gccRelease/RummikubSolverUnitTest.d

# Compiles file stdafx.cpp for the Release configuration...
-include test__gccRelease/stdafx.d
test__gccRelease/stdafx.o: stdafx.cpp
	$(TEST__CPP_COMPILER) $(Test__Release_Preprocessor_Definitions) $(Test__Release_Compiler_Flags) -c stdafx.cpp $(Test__Release_Include_Path) -o test__gccRelease/stdafx.o
	$(TEST__CPP_COMPILER) $(Test__Release_Preprocessor_Definitions) $(Test__Release_Compiler_Flags) -MM stdafx.cpp $(Test__Release_Include_Path) > test__gccRelease/stdafx.d

# Creates the intermediate and output folders for each configuration...
.PHONY: test__create_folders
test__create_folders:
	mkdir -p test__gccDebug/source
	mkdir -p ../test__gccDebug
	mkdir -p test__gccRelease/source
	mkdir -p ../test__gccRelease

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: test__clean
test__clean:
	rm -f test__gccDebug/*.o
	rm -f test__gccDebug/*.d
	rm -f ../test__gccDebug/*.a
	rm -f ../test__gccDebug/*.so
	rm -f ../test__gccDebug/*.dll
	rm -f ../test__gccDebug/*.exe
	rm -f test__gccRelease/*.o
	rm -f test__gccRelease/*.d
	rm -f ../test__gccRelease/*.a
	rm -f ../test__gccRelease/*.so
	rm -f ../test__gccRelease/*.dll
	rm -f ../test__gccRelease/*.exe
	rm -f *.h.gch
