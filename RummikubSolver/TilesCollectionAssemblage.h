/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TILESCOLLECTIONASSEMBLAGE_H
#define TILESCOLLECTIONASSEMBLAGE_H

#include "TilesCollection.h"

class TilesCollectionAssemblage {
public:
    TilesCollectionAssemblage();

    TilesCollectionAssemblage(std::string input);

    TilesCollectionAssemblage(std::string input, bool makeSpecialTC);

    TilesCollectionAssemblage(const TilesCollectionAssemblage &input);

    void add(TilesCollection tc);

    void add(std::string input);

    void addSpecial(std::string input);

    void addTileToEach(Tile t);

    void addTileToEven(Tile t);

    void addTileToOdd(Tile t);

    void removeOne(TilesCollection tc);

    unsigned int size() const NOEXCEPT;

    unsigned int sizeTotal() const NOEXCEPT;

    bool containsTile(Tile t) const;

    unsigned int countTile(Tile t) const;

    unsigned int countTilesCollection(TilesCollection tc) const;

    TilesCollectionAssemblage getDuplicates() const;

    void sort();

    void duplicate();

    bool equals(TilesCollectionAssemblage &other);

    friend std::ostream &operator<<(std::ostream &os, const TilesCollectionAssemblage &tc);

    bool operator==(const TilesCollectionAssemblage &other) const;

    bool operator!=(const TilesCollectionAssemblage &other) const;

    TilesCollection at(std::vector<TilesCollection>::size_type n);

    std::vector<TilesCollection, std::allocator<TilesCollection> >::iterator begin();

    std::vector<TilesCollection, std::allocator<TilesCollection> >::iterator end();

    std::vector<TilesCollection, std::allocator<TilesCollection> >::const_iterator cbegin() const;

    std::vector<TilesCollection, std::allocator<TilesCollection> >::const_iterator cend() const;

    void push_back(const TilesCollection &val);

    void push_back(TilesCollection &&val);

    void reserve(std::vector<TilesCollection>::size_type n);

protected:
    std::vector<TilesCollection>::iterator tcIterator;
    std::vector<TilesCollection> tcArray;
};

#endif
