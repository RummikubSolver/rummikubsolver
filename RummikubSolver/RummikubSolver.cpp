/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <boost/program_options.hpp>

#include "IO.h"
#include "BasicSolver.h"
#include "HeuristicSolver.h"

#define DEFAULT_TIMEOUT 1000
std::size_t timeout;
namespace po = boost::program_options;

int main(int argc, char *argv[]) {

    // Hide input-file
    po::positional_options_description p;
    p.add("input-file", -1);

    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "Show this message")
            ("timeout", po::value<int>()->default_value(DEFAULT_TIMEOUT), "Set timeout (milliseconds).\n"
                                                                          "0 for unlimited timeout.")
            ("solver", po::value<std::string>(), "Which solver to use:\n"
                                                 " rec: \tRecursive "
                                                 "solver which is slow. Doesn't support the timeout "
                                                 "argument.\n"
                                                 " ilp: \tA solver based on "
                                                 "Integer Linear Programming. Not implemented yet\n"
                                                 " heu: \tA heuristic solver.")
            ("input-file", po::value<std::vector<std::string> >(), "input file");


    try {
        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

        if (vm.count("help") || !vm.count("solver")) {
            std::cout << desc << "\n";
            return 1;
        }

        if (vm.count("timeout")) {
            timeout = vm["timeout"].as<int>();
            if (timeout) {
                std::cout << "Timeout used: "
                          << vm["timeout"].as<int>() << " ms.\n";
            } else {
                std::cout << "No timeout limit.\n";
            }
        }

        if (vm["solver"].as<std::string>() == "rec") {
            BasicSolver bSolver;
            IO io;
            TilesCollection inputHand;
            TilesCollection inputTable;
            TilesCollectionAssemblage result;
            io.readTilesFromFile((std::string) (vm["input-file"].as<std::vector<std::string> >()[0]), inputHand,
                                 inputTable);

            result = bSolver.Solve(inputTable, inputHand);
            std::cout << "Result:\n" << result << "\n";
            std::cout << "Uses: "
                << result.sizeTotal()
                << " of " << inputHand.size() + inputTable.size()
                << " tiles total\n";

            std::cout << "Uses: "
                << result.sizeTotal() - inputTable.size()
                << " of " << inputHand.size()
                <<  " tiles from rack\n";


        } else if (vm["solver"].as<std::string>() == "ilp") {
            std::cout << "ILP solver is not yet implemented\n";
        } else if (vm["solver"].as<std::string>() == "heu") {

            HeuristicSolver hSolver;
            IO io;
            TilesCollection inputHand;
            TilesCollectionAssemblage2 inputTable;

            io.readTilesFromFile((std::string) (vm["input-file"].as<std::vector<std::string> >()[0]), inputHand,
                                 inputTable);

            TilesCollectionAssemblage2 result = hSolver.Solve(inputTable, inputHand, timeout);
            std::cout << "result:\n" << result << "\n";
            std::cout << "Uses: " << result.sizeTotal()
                      << " of " << (inputHand.size() + inputTable.sizeTotal())
                      << " tiles total\n";
            std::cout << "Uses: "
                      << result.sizeTotal() - inputTable.sizeTotal()
                      << " of " << inputHand.size()
                      <<  " tiles from rack\n";

        } else {
            std::cout << desc << "\n";
            return 1;
        }
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}

