/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifdef _WIN32

#include "..\RummikubSolver\Tile.cpp"
#include "..\RummikubSolver\TilesCollection.cpp"
#include "..\RummikubSolver\TilesCollectionAssemblage.cpp"
#include "..\RummikubSolver\IO.cpp"
#include "..\RummikubSolver\BasicSolver.cpp"
#include "..\RummikubSolver\HeuristicSolver.cpp"

const std::string testdata_path = "testdata\\";

#else

#include "../RummikubSolver/Tile.cpp"
#include "../RummikubSolver/TilesCollection.cpp"
#include "../RummikubSolver/TilesCollectionAssemblage.cpp"
#include "../RummikubSolver/TilesCollectionAssemblage2.cpp"
#include "../RummikubSolver/IO.cpp"
#include "../RummikubSolver/BasicSolver.cpp"
#include "../RummikubSolver/Run.cpp"
#include "../RummikubSolver/Group.cpp"
#include "../RummikubSolver/Node.cpp"
#include "../RummikubSolver/HeuristicSolver.cpp"

const std::string testdata_path = "testdata/";

#endif

#include "stdafx.h"
#include "gtest/gtest.h"

TEST(testBasicStructures, testTile) {
    for (int color = 1; color <= 4; color++) {
        //Color color = BLUE;
        for (int number = 1; number <= 13; number++) {
            Tile t = Tile((Color) color, number);
            EXPECT_EQ((Color) color, t.getColor());
            EXPECT_EQ(number, t.getNumber());
        }
    }

    //joker
    Color color = NOCOLOR;
    unsigned char number = 0;
    Tile t = Tile((Color) color, number);
    EXPECT_EQ((Color) color, t.getColor());
    EXPECT_EQ(number, t.getNumber());
}

TEST(testBasicStructures, testTilesCollection) {
    Tile t1 = Tile(BLACK, 1);
    Tile t2 = Tile(RED, 2);
    TilesCollection tc;
    tc.add(t1);
    EXPECT_EQ(1, tc.getSize());
    EXPECT_EQ(true, tc.containsColor(BLACK));
    EXPECT_EQ(false, tc.containsColor(RED));
    EXPECT_EQ(true, tc.containsNumber(1));
    EXPECT_EQ(false, tc.containsNumber(2));


    tc.add(t1);
    EXPECT_EQ(2, tc.getSize());

    tc.add(t2);
    EXPECT_EQ(3, tc.getSize());
    EXPECT_EQ(true, tc.containsColor(BLACK));
    EXPECT_EQ(true, tc.containsColor(RED));
    EXPECT_EQ(false, tc.containsColor(NOCOLOR));
    EXPECT_EQ(true, tc.containsNumber(1));
    EXPECT_EQ(true, tc.containsNumber(2));
    EXPECT_EQ(false, tc.containsNumber(0));

    tc.removeOne(t1);
    EXPECT_EQ(2, tc.getSize());

    tc.add(t1);
    EXPECT_EQ(3, tc.getSize());

    tc.removeAll(t1);
    EXPECT_EQ(1, tc.getSize());
}

TEST(testOutput, testTilesCollection) {
    Tile t1 = Tile(BLACK, 1);
    Tile t2 = Tile(RED, 2);
    TilesCollection tc;
    tc.add(t1);
    tc.add(t2);
    std::cout << tc << "\n";

}

TEST(testBasicStructures, testGetNonExistingColors) {
TilesCollection tc1("[405][305][205][105]");
Group g1(tc1);
EXPECT_TRUE(g1.getNonExistingColors().empty());

TilesCollection tc2("[305][205][105]");
Group g2(tc2);
auto nonExistingColors = g2.getNonExistingColors();
EXPECT_EQ(1,nonExistingColors.size());
EXPECT_EQ(BLACK, nonExistingColors.back());
}

TEST(testTilesCollection, testJoinTilesCollections) {
    TilesCollection a;
    TilesCollection b;
    TilesCollection c;
    TilesCollection d;

    Tile t1(BLUE, 1);
    a.add(t1);
    Tile t2(BLUE, 2);
    a.add(t2);
    Tile t3(BLUE, 3);
    a.add(t3);

    Tile t4(RED, 1);
    b.add(t4);
    Tile t5(RED, 2);
    b.add(t5);
    Tile t6(RED, 3);
    b.add(t6);

    c = TilesCollection::joinTilesCollections(a, b);

    EXPECT_NE(c,d);
    d.add(t1);
    d.add(t4);
    d.add(t2);
    d.add(t5);
    d.add(t3);
    EXPECT_NE(c,d);
    d.add(t6);
    EXPECT_EQ(c,d);
}

TEST(testTilesCollection, testRemoveTiles) {
    TilesCollection a;
    TilesCollection b;
    TilesCollection c;
    TilesCollection d;

    Tile t1(BLUE, 1);
    a.add(t1);
    Tile t2(BLUE, 2);
    a.add(t2);
    Tile t3(BLUE, 2);
    a.add(t3);

    Tile t4(BLUE, 2);
    b.add(t4);
    Tile t5(BLUE, 2);
    b.add(t5);

    a.removeTiles(b);

    EXPECT_EQ(a.size(),1);
    EXPECT_TRUE(a.containsTile(t1));


    //205205 bug
    TilesCollection e("[105][205][305][405]");
    TilesCollection f("[410][411][412][413][105][205][305][205][206][207][405][204]");
    TilesCollection g("[410][411][412][413][205][206][207][204]");

    f.removeTiles(e);
    EXPECT_EQ(g,f);

}


TEST(testBasicStructures, testTilesCollectionAssemblage) {

    Tile t1 = Tile(BLACK, 1);
    Tile t2 = Tile(RED, 2);
    TilesCollection tc1;
    tc1.add(t1);
    tc1.add(t2);

    Tile t3 = Tile(BLUE, 3);
    Tile t4 = Tile(YELLOW, 4);
    TilesCollection tc2;
    tc2.add(t3);
    //tc2.add(t4);

    TilesCollectionAssemblage tca;
    tca.add(tc1);
    EXPECT_EQ(1, tca.size());
    tca.add(tc2);
    EXPECT_EQ(2, tca.size());

    EXPECT_TRUE(tca.containsTile(t1));
    EXPECT_FALSE(tca.containsTile(t4));
}


TEST(testTilesCollectionAssemblage, testDuplicate) {

    TilesCollection tc1();
    TilesCollection tc2();

    TilesCollectionAssemblage tca("101,102,000/405,405,302");
    TilesCollectionAssemblage tcaCompare("101,102,000/405,405,302/101,102,000/405,405,302");

    EXPECT_EQ(2, tca.size());
    tca.duplicate();
    EXPECT_EQ(4, tca.size());
    //std::cout << tca << "\n";
    EXPECT_TRUE(tca.equals(tcaCompare));

}


TEST(testTilesCollectionAssemblage, testgetDuplicates) {

    TilesCollectionAssemblage tca("101,102,000/101,102,000/405,406,407");
    TilesCollectionAssemblage tcaCompare("101,102,000");
    TilesCollectionAssemblage result;

    result = tca.getDuplicates();
    EXPECT_EQ(1, result.size());
    EXPECT_TRUE(result.equals(tcaCompare)) << "result: \n" << result << "\n";
}


TEST(testIO, testReadTilesFromFile) {
    IO io;
    TilesCollection comparisonRack;
    TilesCollection rack;
    TilesCollection table;

    //test an empty file
    io.readTilesFromFile(testdata_path + "testcase1.txt", rack, table);
    EXPECT_EQ(0, rack.getSize());
    EXPECT_EQ(0, table.getSize());

    //test file with comments and empty lines
    io.readTilesFromFile(testdata_path + "testcase2.txt", rack, table);
    EXPECT_EQ(0, rack.getSize());
    EXPECT_EQ(0, table.getSize());

    //normal file, 5 in rack, 10 in table
    //rack:  000,112,113,203,204
    //table: 401,402,403,404,405,406,101,201,301,401
    Tile t1(NOCOLOR, 0);
    comparisonRack.add(t1);
    Tile t2(RED, 12);
    comparisonRack.add(t2);
    Tile t3(RED, 13);
    comparisonRack.add(t3);
    Tile t4(BLUE, 3);
    comparisonRack.add(t4);
    Tile t5(BLUE, 4);
    comparisonRack.add(t5);

    io.readTilesFromFile(testdata_path + "testcase3.txt", rack, table);
    EXPECT_EQ(5, rack.getSize());
    EXPECT_EQ(10, table.getSize());


    for (std::vector<Tile>::reverse_iterator it = rack.rbegin(); it != rack.rend(); ++it) {
        if (comparisonRack.containsTile(*it)) {
            comparisonRack.removeOne(*it);
        }
    }
    EXPECT_EQ(0, comparisonRack.getSize());

}


TEST(testBasicSolver, testBasicSolverGetPossibleCombinations) {
    IO io;
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    io.readTilesFromFile(testdata_path + "testcase4.txt", rack, table);
    EXPECT_EQ(5, rack.getSize());
    EXPECT_EQ(3, table.getSize());


    result = bSolver.getPossibleCombinations(table, rack);
    EXPECT_EQ(11, result.size());

    TilesCollectionAssemblage compareTCA(
            "[404][405][406][407][408]/"
            "[405][406][407][408]/"
            "[406][407][408]/"
            "[404][405][406][407]/"
            "[405][406][407]/"
            "[404][405][406]/"
            "[307][207][107]/"
            "[407][207][107]/"
            "[407][307][107]/"
            "[407][307][207]/"
            "[407][307][207][107]"
    );

    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";

    EXPECT_TRUE(compareTCA.equals(result));
}

TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsGroupsNoJoker) {
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    table = TilesCollection("101,201,301");
    rack = TilesCollection("401");
    result = bSolver.getPossibleCombinations(rack, table);
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(5, result.size());

    TilesCollectionAssemblage compareTCA(
            "[101][201][301]/"
            "[101][201][401]/"
            "[101][301][401]/"
            "[201][301][401]/"
            "[101][201][301][401]"
    );

    EXPECT_TRUE(compareTCA.equals(result));
}

TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsGroupsOneJoker) {
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    table = TilesCollection("101,201,301");
    rack = TilesCollection("401,000");
    result = bSolver.getPossibleCombinations(rack, table);
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(15, result.size());

    TilesCollectionAssemblage compareTCA(
            "[000][101][201]/"
            "[000][101][301]/"
            "[000][101][401]/"
            "[000][201][301]/"
            "[000][201][401]/"
            "[000][301][401]/"
            "[101][201][301]/"
            "[101][201][401]/"
            "[101][301][401]/"
            "[201][301][401]/"
            "[000][101][201][301]/"
            "[000][101][201][401]/"
            "[000][101][301][401]/"
            "[000][201][301][401]/"
            "[101][201][301][401]"
    );

    EXPECT_TRUE(compareTCA.equals(result));

}

TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsGroupsTwoJokers) {
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    table = TilesCollection("101,201,301");
    rack = TilesCollection("401,000,000");
    result = bSolver.getPossibleCombinations(rack, table);
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(25, result.size());

    TilesCollectionAssemblage compareTCA(
            "[000][000][101]/"
            "[000][000][201]/"
            "[000][000][301]/"
            "[000][000][401]/"
            "[000][101][201]/"
            "[000][101][301]/"
            "[000][101][401]/"
            "[000][201][301]/"
            "[000][201][401]/"
            "[000][301][401]/"
            "[101][201][301]/"
            "[101][201][401]/"
            "[101][301][401]/"
            "[201][301][401]/"
            "[000][000][101][201]/"
            "[000][000][101][301]/"
            "[000][000][101][401]/"
            "[000][000][201][301]/"
            "[000][000][201][401]/"
            "[000][000][301][401]/"
            "[000][101][201][301]/"
            "[000][101][201][401]/"
            "[000][101][301][401]/"
            "[000][201][301][401]/"
            "[101][201][301][401]"
    );

    EXPECT_TRUE(compareTCA.equals(result));
}


TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsRunsOneJoker) {
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    table = TilesCollection("105,106,107");
    rack = TilesCollection("108,000");
    result = bSolver.getPossibleCombinations(rack, table);
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(13, result.size());


    TilesCollectionAssemblage compareTCA(
            "[000][105][106]/"
            "[000][105][107]/"
            "[000][106][107]/"
            "[000][106][108]/"
            "[000][107][108]/"
            "[105][106][107]/"
            "[106][107][108]/"
            "[000][105][106][107]/"
            "[000][105][106][108]/"
            "[000][105][107][108]/"
            "[000][106][107][108]/"
            "[105][106][107][108]/"
            "[000][105][106][107][108]"

    );

    EXPECT_TRUE(compareTCA.equals(result));
}


TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsRunsTwoJokers) {
    //TilesCollection rack;
    //TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    result = bSolver.getPossibleCombinations(TilesCollection("105,106,107,108,000,000"));
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(27, result.size());

    TilesCollectionAssemblage compareTCA(
            "[000][000][105]/"
            "[000][000][106]/"
            "[000][000][107]/"
            "[000][000][108]/"
            "[000][105][106]/"
            "[000][105][107]/"
            "[000][106][107]/"
            "[000][106][108]/"
            "[000][107][108]/"
            "[105][106][107]/"
            "[106][107][108]/"
            "[000][000][105][106]/"
            "[000][000][105][107]/"
            "[000][000][105][108]/"
            "[000][000][106][107]/"
            "[000][000][106][108]/"
            "[000][000][107][108]/"
            "[000][105][106][107]/"
            "[000][105][106][108]/"
            "[000][105][107][108]/"
            "[000][106][107][108]/"
            "[105][106][107][108]/"
            "[000][000][105][106][107]/"
            "[000][000][105][106][108]/"
            "[000][000][105][107][108]/"
            "[000][000][106][107][108]/"
            "[000][105][106][107][108]"
    );

    EXPECT_TRUE(compareTCA.equals(result));

}


TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsAllTwoJokers) {
    TilesCollection inputTiles;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    inputTiles = TilesCollection(
            "000,000,"
            "000,000,"
            "101,102,103,104,105,106,107,108,109,110,111,112,113,"
            "101,102,103,104,105,106,107,108,109,110,111,112,113,"
            "201,202,203,204,205,206,207,208,209,210,211,212,213,"
            "201,202,203,204,205,206,207,208,209,210,211,212,213,"
            "301,302,303,304,305,306,307,308,309,310,311,312,313,"
            "301,302,303,304,305,306,307,308,309,310,311,312,313,"
            "401,402,403,404,405,406,407,408,409,410,411,412,413,"
            "401,402,403,404,405,406,407,408,409,410,411,412,413"
    );

    result = bSolver.getPossibleCombinations(inputTiles);
    //NOTE! we _have_ to sort this for the group/run matching down to work.
    result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(1173, result.size()); //according to ILP paper //which had an error

    int threeRun = 0;
    int threeRun1J = 0;
    int threeRun2J = 0;
    int fourRun = 0;
    int fourRun1J = 0;
    int fourRun2J = 0;
    int fiveRun = 0;
    int fiveRun1J = 0;
    int fiveRun2J = 0;
    int threeGroup = 0;
    int threeGroup1J = 0;
    int threeGroup2J = 0;
    int fourGroup = 0;
    int fourGroup1J = 0;
    int fourGroup2J = 0;
    for (std::vector<TilesCollection>::iterator it = result.begin(); it != result.end(); ++it) {
        if ((*it).at((*it).size() - 2).getColor() == (*it).at((*it).size() - 1).getColor()) {
            if ((*it).size() == 3) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    threeRun++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    threeRun1J++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 2) {
                    threeRun2J++;
                }
            }
            if ((*it).size() == 4) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    fourRun++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    fourRun1J++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 2) {
                    fourRun2J++;
                }
            }
            if ((*it).size() == 5) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    fiveRun++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    fiveRun1J++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 2) {
                    fiveRun2J++;
                }
            }
        } else {
            if ((*it).size() == 3) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    threeGroup++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    threeGroup1J++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 2) {
                    threeGroup2J++; //So this is technically the same as threeRun2J
                }
            }
            if ((*it).size() == 4) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    fourGroup++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    fourGroup1J++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 2) {
                    fourGroup2J++;
                }
            }
        }
    }

    EXPECT_EQ(44, threeRun);
    EXPECT_EQ(92, threeRun1J);
    EXPECT_EQ(0, threeRun2J);
    //EXPECT_EQ(52,threeRun2J);

    EXPECT_EQ(40, fourRun);
    EXPECT_EQ(124, fourRun1J);
    EXPECT_EQ(132, fourRun2J);

    EXPECT_EQ(36, fiveRun);
    EXPECT_EQ(148, fiveRun1J);
    EXPECT_EQ(232, fiveRun2J); //Error in ILP paper

    EXPECT_EQ(52, threeGroup);
    EXPECT_EQ(78, threeGroup1J);
    EXPECT_EQ(52, threeGroup2J);
    //EXPECT_EQ(0,threeGroup2J);

    EXPECT_EQ(13, fourGroup);
    EXPECT_EQ(52, fourGroup1J);
    EXPECT_EQ(78, fourGroup2J);

    TilesCollectionAssemblage duplicates;
    duplicates = result.getDuplicates();
    EXPECT_EQ(0, duplicates.size()) << "Duplicates:\n" << duplicates << "\n";
}


TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsAllOneJoker) {
    TilesCollection inputTiles;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    inputTiles = TilesCollection(
            "000,"
            "101,102,103,104,105,106,107,108,109,110,111,112,113,"
            "101,102,103,104,105,106,107,108,109,110,111,112,113,"
            "201,202,203,204,205,206,207,208,209,210,211,212,213,"
            "201,202,203,204,205,206,207,208,209,210,211,212,213,"
            "301,302,303,304,305,306,307,308,309,310,311,312,313,"
            "301,302,303,304,305,306,307,308,309,310,311,312,313,"
            "401,402,403,404,405,406,407,408,409,410,411,412,413,"
            "401,402,403,404,405,406,407,408,409,410,411,412,413"
    );

    result = bSolver.getPossibleCombinations(inputTiles);
    //NOTE! we _have_ to sort this for the group/run matching down to work.
    result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(679, result.size()); //according to ILP paper

    int threeRun = 0;
    int threeRun1J = 0;
    int fourRun = 0;
    int fourRun1J = 0;
    int fiveRun = 0;
    int fiveRun1J = 0;
    int threeGroup = 0;
    int threeGroup1J = 0;
    int fourGroup = 0;
    int fourGroup1J = 0;
    for (std::vector<TilesCollection>::iterator it = result.begin(); it != result.end(); ++it) {
        if ((*it).at((*it).size() - 2).getColor() == (*it).at((*it).size() - 1).getColor()) {
            if ((*it).size() == 3) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    threeRun++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    threeRun1J++;
                }
            }
            if ((*it).size() == 4) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    fourRun++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    fourRun1J++;
                }
            }
            if ((*it).size() == 5) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    fiveRun++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    fiveRun1J++;
                }
            }
        } else {
            if ((*it).size() == 3) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    threeGroup++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    threeGroup1J++;
                }
            }
            if ((*it).size() == 4) {
                if ((*it).countTile(Tile(Color(0), 0)) == 0) {
                    fourGroup++;
                }
                if ((*it).countTile(Tile(Color(0), 0)) == 1) {
                    fourGroup1J++;
                }
            }
        }
    }

    EXPECT_EQ(44, threeRun);
    EXPECT_EQ(92, threeRun1J);

    EXPECT_EQ(40, fourRun);
    EXPECT_EQ(124, fourRun1J);

    EXPECT_EQ(36, fiveRun);
    EXPECT_EQ(148, fiveRun1J);

    EXPECT_EQ(52, threeGroup);
    EXPECT_EQ(78, threeGroup1J);

    EXPECT_EQ(13, fourGroup);
    EXPECT_EQ(52, fourGroup1J);
}


TEST(testBasicSolver, testBasicSolverGetPossibleCombinationsAllNoJokers) {
    TilesCollection inputTiles;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    inputTiles = TilesCollection(
            "101,102,103,104,105,106,107,108,109,110,111,112,113,"
            "101,102,103,104,105,106,107,108,109,110,111,112,113,"
            "201,202,203,204,205,206,207,208,209,210,211,212,213,"
            "201,202,203,204,205,206,207,208,209,210,211,212,213,"
            "301,302,303,304,305,306,307,308,309,310,311,312,313,"
            "301,302,303,304,305,306,307,308,309,310,311,312,313,"
            "401,402,403,404,405,406,407,408,409,410,411,412,413,"
            "401,402,403,404,405,406,407,408,409,410,411,412,413"
    );

    result = bSolver.getPossibleCombinations(inputTiles);
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(185, result.size()); //according to ILP paper

    int countRuns = 0;
    int countGroups = 0;
    int threeRun = 0;
    int fourRun = 0;
    int fiveRun = 0;
    int threeGroup = 0;
    int fourGroup = 0;
    for (std::vector<TilesCollection>::iterator it = result.begin(); it != result.end(); ++it) {
        if ((*it).at(0).getColor() == (*it).at(1).getColor()) {
            countRuns++;
            if ((*it).size() == 3)threeRun++;
            if ((*it).size() == 4)fourRun++;
            if ((*it).size() == 5)fiveRun++;
        } else {
            countGroups++;
            if ((*it).size() == 3)threeGroup++;
            if ((*it).size() == 4)fourGroup++;
        }
    }

    EXPECT_EQ(120, countRuns);
    EXPECT_EQ(65, countGroups);

    EXPECT_EQ(44, threeRun);
    EXPECT_EQ(40, fourRun);
    EXPECT_EQ(36, fiveRun);

    EXPECT_EQ(52, threeGroup);
    EXPECT_EQ(13, fourGroup);


}

/*This one recreates a specific bug*/
TEST(testBasicSolver, testBasicSolverSolveBug205205) {
    IO io;
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    io.readTilesFromFile(testdata_path + "testcase9.txt", rack, table);
    EXPECT_EQ(2, rack.getSize());
    EXPECT_EQ(10, table.getSize());

    result = bSolver.Solve(table, rack);

    Tile blue5(BLUE, 5);
    int foundBlue5 = 0;
    foundBlue5 = result.countTile(blue5);
    EXPECT_EQ(2, foundBlue5);
    //result.removeOne(blue5);
    for (std::vector<TilesCollection>::iterator it = result.begin(); it != result.end(); ++it) {
        if ((*it).containsTile(blue5)) {
            (*it).removeOne(blue5);
            break;
        }
    }

    foundBlue5 = result.countTile(blue5);
    EXPECT_EQ(1, foundBlue5);
    //result.removeOne(blue5);
    for (std::vector<TilesCollection>::iterator it = result.begin(); it != result.end(); ++it) {
        if ((*it).containsTile(blue5)) {
            (*it).removeOne(blue5);
        }
    }
    foundBlue5 = result.countTile(blue5);
    EXPECT_EQ(0, foundBlue5);
}

TEST(testHeuristicSolver, testBasicSolverSolveBug205205) {
    IO io;
    TilesCollection rack;
    TilesCollectionAssemblage2 table;
    TilesCollectionAssemblage2 result;
    HeuristicSolver hSolver;

    io.readTilesFromFile(testdata_path + "testcase9.txt", rack, table);
    EXPECT_EQ(2, rack.getSize());
    EXPECT_EQ(10, table.sizeTotal());

    result = hSolver.Solve(table, rack);

    Tile blue5(BLUE, 5);
    int foundBlue5 = 0;
    foundBlue5 = result.countTile(blue5);
    EXPECT_EQ(2, foundBlue5);
    //result.removeOne(blue5);
    for (auto it = result.begin(); it != result.end(); ++it) {
        if ((*it)->containsTile(blue5)) {
            (*it)->removeOne(blue5);
            break;
        }
    }

    foundBlue5 = result.countTile(blue5);
    EXPECT_EQ(1, foundBlue5);
    //result.removeOne(blue5);
    for (auto it = result.begin(); it != result.end(); ++it) {
        if ((*it)->containsTile(blue5)) {
            (*it)->removeOne(blue5);
        }
    }
    foundBlue5 = result.countTile(blue5);
    EXPECT_EQ(0, foundBlue5);
}


TEST(testBasicSolver, testSolveR3T40)
{
	IO io;
	TilesCollection rack;
	TilesCollection table;
	TilesCollectionAssemblage result;
	BasicSolver bSolver;
	
	io.readTilesFromFile(testdata_path + "testcase8.txt", rack, table);
	EXPECT_EQ(3,rack.getSize());
	EXPECT_EQ(40,table.getSize());
	
	result = bSolver.Solve(table,rack);
	//result.sort();
	//std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(43, result.sizeTotal());
}

TEST(testHeuristicSolver, testSolveR3T40)
{
    IO io;
    TilesCollection rack;
    TilesCollectionAssemblage2 table;
    TilesCollectionAssemblage2 result;
    HeuristicSolver hSolver;

    io.readTilesFromFile(testdata_path + "testcase8.txt", rack, table);
    EXPECT_EQ(3,rack.getSize());
    EXPECT_EQ(40,table.sizeTotal());

    result = hSolver.Solve(table,rack);
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_EQ(43, result.sizeTotal());
}


/*
//too slow (some hours?)
TEST(testBasicSolver, testSolveR5T41TwoJoker)
{
	IO io;
	TilesCollection rack;
	TilesCollection table;
	TilesCollectionAssemblage result;
	BasicSolver bSolver;
	
	io.readTilesFromFile(testdata_path + "testcase10.txt", rack, table);
	EXPECT_EQ(5,rack.getSize());
	EXPECT_EQ(40,table.getSize());
	
	result = bSolver.Solve(table,rack);
	result.sort();
	std::cout << " \n final result: \n" << result << "\n";
}
*/


TEST(testHeuristicSolver, testSolveR5T41TwoJoker)
{
    IO io;
    TilesCollection rack;
    TilesCollectionAssemblage2 table;
    TilesCollectionAssemblage2 result;
    HeuristicSolver hSolver;

    io.readTilesFromFile(testdata_path + "testcase10.txt", rack, table);
    EXPECT_EQ(5,rack.getSize());
    EXPECT_EQ(40,table.sizeTotal());

    result = hSolver.Solve(table,rack,8000);
    //result.sort();
    //std::cout << " \n final result: \n" << result << "\n";
    EXPECT_GE(41, result.sizeTotal());
}


TEST(testBasicSolver, testSolveR4T42OneJoker)
{
	IO io;
	TilesCollection rack;
	TilesCollection table;
	TilesCollectionAssemblage result;
	BasicSolver bSolver;
	
	io.readTilesFromFile(testdata_path + "testcase11.txt", rack, table);
	EXPECT_EQ(4,rack.getSize());
	EXPECT_EQ(43,table.getSize());
	
	result = bSolver.Solve(table,rack);
	//result.sort();
	//std::cout << "final result: \n" << result << "\n";
    EXPECT_EQ(47, result.sizeTotal());
}

TEST(testHeuristicSolver, testSolveR4T42OneJoker)
{
    IO io;
    TilesCollection rack;
    TilesCollectionAssemblage2 table;
    TilesCollectionAssemblage2 result;
    HeuristicSolver hSolver;

    io.readTilesFromFile(testdata_path + "testcase11.txt", rack, table);
    EXPECT_EQ(4,rack.getSize());
    EXPECT_EQ(43,table.sizeTotal());

    result = hSolver.Solve(table,rack);
    //result.sort();
    //std::cout << "final result: \n" << result << "\n";
    EXPECT_EQ(47, result.sizeTotal());
}

// Somehow only heu algorithm found a solution for this.
// Problem was that joker was introduced with a number "005" instead of "000"
TEST(testBasicSolver, testHeuPaperTest7)
{
    IO io;
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    io.readTilesFromFile(testdata_path + "testcase13.txt", rack, table);
    EXPECT_EQ(5,rack.getSize());
    EXPECT_EQ(3,table.getSize());

    result = bSolver.Solve(table,rack);
    //result.sort();
    //std::cout << "final result: \n" << result << "\n";
    EXPECT_EQ(8, result.sizeTotal());
}

// heu algorithm doesn't find optimal solution for this?
TEST(testHeuristicSolver, testHeuPaperTest1)
{
    IO io;
    TilesCollection rack;
    TilesCollectionAssemblage2 table;
    TilesCollectionAssemblage2 result;
    HeuristicSolver hSolver;

    io.readTilesFromFile(testdata_path + "testcase14.txt", rack, table);
    // [210],[310]
    EXPECT_EQ(2,rack.getSize());
    // [107],[108],[109],[110],[111],[112],[113]
    EXPECT_EQ(7,table.sizeTotal());

    result = hSolver.Solve(table,rack,0); // 0 timeout for debugging purposes
    result.sort();
    //std::cout << "final result: \n" << result << "\n";
    EXPECT_EQ(9, result.sizeTotal()) << "result: \n" << result << "\n";
    // basically, get the 110 away from the middle and into its own group
    // 107,108,109/111,112,113/220,210,310
    // [107],[108],[109]/[111],[112],[113]/[220],[210],[310]
}

TEST(testBasicSolver, testHeuPaperTest1)
{
    IO io;
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    io.readTilesFromFile(testdata_path + "testcase14.txt", rack, table);
    EXPECT_EQ(2,rack.getSize());
    EXPECT_EQ(7,table.getSize());

    result = bSolver.Solve(table,rack);
    result.sort();
    //std::cout << "final result: \n" << result << "\n";
    EXPECT_EQ(9, result.sizeTotal())  << "result: \n" << result << "\n";
}



/*
 * // Too slow (like multiple weeks slow...)
TEST(testBasicSolver, testSolveILP1R9T24OneJoker)
{
	IO io;
	TilesCollection rack;
	TilesCollection table;
	TilesCollectionAssemblage result;
	BasicSolver bSolver;
	
	io.readTilesFromFile(testdata_path + "testcase6.txt", rack, table);
	EXPECT_EQ(9,rack.getSize());
	EXPECT_EQ(24,table.getSize());
	
	result = bSolver.Solve(table,rack);
	result.sort();
	std::cout << "final result: \n" << result << "\n";
    //EXPECT_EQ(1333337,result.sizeTotal());
}
*/

TEST(testHeuristicSolver, testSolveILP1R9T24OneJoker)
{
	IO io;
	TilesCollection rack;
    TilesCollectionAssemblage2 table;
	TilesCollectionAssemblage2 result;
	HeuristicSolver hSolver;

	io.readTilesFromFile(testdata_path + "testcase6.txt", rack, table);
	EXPECT_EQ(9,rack.getSize());
	EXPECT_EQ(24,table.sizeTotal());

	result = hSolver.Solve(table,rack,2000);

    // Seems like this is very timesensitive. When running all test cases this case returns 26 tiles
    // but only running this case separately finds 28... edit: now 29
    EXPECT_LE(result.sizeTotal(),29) << "result:\n" << result << "\n";
}


/*
TEST(testBasicSolver, testSolvebluuh)
{
	IO io;
	TilesCollection rack("000,101,201");
	TilesCollection table("401,402,403,404,405,406,407,408,409,410,411,412,413,105,205,305,405,101,102,103,104,105,106,107,109,110,111,112,113");
	TilesCollectionAssemblage result;
	BasicSolver bSolver;
	
	result = bSolver.Solve(table,rack);
	result.sort();
	std::cout << "final result: \n" << result << "\n";
}
*/

/*
TEST(testBasicSolver, testSolveNoJokersLong)
{
	IO io;
	//9247690 ms ~ 2.5h
	TilesCollection rack("108,304,404,104");
	TilesCollection table("401,402,403,405,406,407,408,409,410,411,412,413,105,205,305,405,101,102,103,104,105,106,107,109,110,111,112,113,104,204,404,109,209,309,409,101,102,103");
	
	//20s
	//TilesCollection rack("108,304,404");
	//TilesCollection table("401,402,403,405,406,407,408,409,410,411,412,413,105,205,305,405,101,102,103,104,105,106,107,109,110,111,112,113,104,204,304,404,109,209,309,409");
	
	//24s
	//TilesCollection rack("108,304,404");
	//TilesCollection table("401,402,403,405,406,407,408,409,410,411,412,413,105,205,305,405,101,102,103,104,105,106,107,109,110,111,112,113,104,204,304,404,109,209,309,409,101,102,103,201,202,203");
	
	//doesn't stop?
	//TilesCollection rack("108,304,404");
	//TilesCollection table("401,402,403,405,406,407,408,409,410,411,412,413,105,205,305,405,101,102,103,104,105,106,107,109,110,111,112,113,104,204,304,404,109,209,309,409,101,102,103,201,202,203,204");
	
	//73235 ms
	//TilesCollection rack("108,304,404");
	//TilesCollection table("401,402,403,105,205,305,405,101,102,103,104,105,106,107,109,110,111,112,113,104,204,304,404,109,209,309,409,101,102,103,201,202,203,204");
	
	// 193014 ms
	//TilesCollection rack("108,304,404,405");
	//TilesCollection table("401,402,403,105,205,305,405,101,102,103,104,105,106,107,109,110,111,112,113,104,204,304,404,109,209,309,409,101,102,103,201,202,203,204");
	
	
	TilesCollectionAssemblage result;
	BasicSolver bSolver;
	
	result = bSolver.Solve(table,rack);
	result.sort();
	std::cout << "final result: \n" << result << "\n";
}
*/

TEST(testBasicSolver, testSolveDuplicateRuns)
{
    IO io;
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    io.readTilesFromFile(testdata_path + "testcase15.txt", rack, table);

    result = bSolver.Solve(table,rack);
    result.sort();
    EXPECT_EQ(result.sizeTotal(),8) << "result:\n" << result << "\n";
    //std::cout << "final result: \n" << result << "\n";
}

TEST(testHeuristicSolver, testSolveDuplicateRuns)
{
    IO io;
    TilesCollection rack;
    TilesCollectionAssemblage2 table;
    TilesCollectionAssemblage2 result;
    HeuristicSolver hSolver;

    io.readTilesFromFile(testdata_path + "testcase15.txt", rack, table);

    result = hSolver.Solve(table,rack,5000);
    result.sort();
    EXPECT_EQ(result.sizeTotal(),8) << "result:\n" << result << "\n";
    //std::cout << "final result: \n" << result << "\n";
}

TEST(testBasicSolver, testSolveDuplicateGroups)
{
    IO io;
    TilesCollection rack;
    TilesCollection table;
    TilesCollectionAssemblage result;
    BasicSolver bSolver;

    io.readTilesFromFile(testdata_path + "testcase16.txt", rack, table);

    result = bSolver.Solve(table,rack);
    result.sort();
    EXPECT_EQ(result.sizeTotal(),8) << "result:\n" << result << "\n";
    //std::cout << "final result: \n" << result << "\n";
}

TEST(testHeuristicSolver, testSolveDuplicateGroups)
{
    IO io;
    TilesCollection rack;
    TilesCollectionAssemblage2 table;
    TilesCollectionAssemblage2 result;
    HeuristicSolver hSolver;

    io.readTilesFromFile(testdata_path + "testcase16.txt", rack, table);

    result = hSolver.Solve(table,rack,5000);
    result.sort();
    EXPECT_EQ(result.sizeTotal(),8) << "result:\n" << result << "\n";
    //std::cout << "final result: \n" << result << "\n";
}


TEST(testHeuristicSolver, testNodeHash) {
    HeuristicSolver hSolver;

    //TODO stop using stupid hacks
    TilesCollectionAssemblage2 tca(

#include "../RummikubSolver/1773TilesCollections.inc"

    );

    std::vector<size_t> hashes;


    for (auto tc: tca) {
        // Set amount of tiles we start with. Otherwise this will trigger debug error logs
        hSolver.setCardCount(tc->getSize());

        // Put the tilescollection into a TilesCollectionAssemblage
        TilesCollectionAssemblage2 temp_tca = TilesCollectionAssemblage2();
        temp_tca.add(*tc);
        Node test_node = Node(nullptr, temp_tca, 0, &hSolver);

        // Create hash value from the TilesCollectionAssemblage and add it to vector
        hashes.push_back(Node::hash_value(test_node));
        //std::cout << Node::hash_value(test_node) << "\t\t";
        //std::cout << *tc << "\n";
    }

    // Check if we have duplicate hash values. Need to sort to be able to use adjacent_find!
    sort(hashes.begin(), hashes.end());
    //EXPECT_TRUE(adjacent_find(hashes.begin(), hashes.end()) == hashes.end());
    EXPECT_EQ(adjacent_find(hashes.begin(), hashes.end()), hashes.end());

    // Filter all non-unique hashes and print them
    sort(hashes.begin(), hashes.end());
    std::set<size_t> uvec(hashes.begin(), hashes.end());
    std::list<size_t> output;
    set_difference(hashes.begin(), hashes.end(),
                   uvec.begin(), uvec.end(),
                   back_inserter(output));
    if (output.size() > 0) {
        std::cout << "These hashes has collisions:\n";
        for (auto hash:output)
            std::cout << hash << "\n";
        std::cout << "End hash list\n";

    } else {
        std::cout << "No hashes with collisions found.\n";
    }

}

TEST(testHeuristicSolver, testHeuristicSolverSolveBasicRuns) {
    //TODO when running Solve() we should clear out old things, actually we probably want a destructor
    HeuristicSolver hSolver;


    TilesCollection inputHand(
            "104,105"
    );

    TilesCollectionAssemblage2 inputTable(
            "101,102,103/106,107,108"
    );

    TilesCollectionAssemblage2 result = hSolver.Solve(inputTable, inputHand,2000);

    //std::cout << "result:\n" << result2 << "\n";
    EXPECT_EQ(8, result.sizeTotal());
}

TEST(testHeuristicSolver, testHeuristicSolverSolveBasicGroups) {
    HeuristicSolver hSolver2;

    TilesCollection inputHand2(
            "401,106"
    );

    TilesCollectionAssemblage2 inputTable2(
            "101,201,301/206,306,406"
    );

    TilesCollectionAssemblage2 result2 = hSolver2.Solve(inputTable2, inputHand2);

    //std::cout << "result:\n" << result2 << "\n";
    EXPECT_EQ(8, result2.sizeTotal());

}

TEST(testHeuristicSolver, testHeuristicSolverSolveBasicRunsAndGroups) {
    HeuristicSolver hSolver3;

    TilesCollection inputHand3(
            "401,106,104,105"
    );

    TilesCollectionAssemblage2 inputTable3(
            "101,201,301/206,306,406/101,102,103/106,107,108"
    );

    TilesCollectionAssemblage2 result3 = hSolver3.Solve(inputTable3, inputHand3);

    //std::cout << "result:\n" << result3 << "\n";
    EXPECT_EQ(16, result3.sizeTotal());
}


TEST(testHeuristicSolver, testHeuristicSolverSolveRunsJokerInRack) {
    HeuristicSolver hSolver;


    TilesCollection inputHand(
            "104,105,000"
    );

    TilesCollectionAssemblage2 inputTable(
            "101,102,103/106,107,108"
    );

    TilesCollectionAssemblage2 result = hSolver.Solve(inputTable, inputHand);

    //std::cout << "result:\n" << result << "\n";
    EXPECT_EQ(9, result.sizeTotal());
}

TEST(testHeuristicSolver, testHeuristicSolverSolveLong1) {
    HeuristicSolver hSolver;


    TilesCollection inputHand(
            "108,304,404,104"
    );

    TilesCollectionAssemblage2 inputTable(
            "401,402,403/405,406,407,408,409,410,411,412,413/105,205,305,405/101,102,103,104,105,106,107/109,110,111,112,113/104,204,404/109,209,309,409/101,102,103"
    );


    TilesCollectionAssemblage2 result = hSolver.Solve(inputTable, inputHand);

    //std::cout << "result:\n" << result2 << "\n";
    EXPECT_EQ(42, result.sizeTotal());
}

//ILP is from the ILP paper
TEST(testHeuristicSolver, testHeuristicSolverSolveILP1R9T24OneJoker) {
    HeuristicSolver hSolver;

    //m1hsS = "B1 B3 B4 B8 R2 K2 K10 K13 J";
    // 9 tiles
    TilesCollection inputHand(
            "101,103,104,108,202,402,410,413,000"
    );

    //m1bsS = "K1-4 K10-12 B6-8 B9-13 R7-9 R10-12 O11-13";
    // 24 tiles
    TilesCollectionAssemblage2 inputTable(
            "401,402,403,404/410,411,412/106,107,108,109,110,111,112,113/207,208,209,210,211,212/311,312,313"
    );


    // Note: timeout 50000 gives better result (29)
    TilesCollectionAssemblage2 result = hSolver.Solve(inputTable, inputHand,1000);

    //m1beS = "K1-4 K10-13 B6-8 B8-10 B11-13 R7-9 R10-12 O11-13 B1-4J2";
    // 30 tiles
    TilesCollectionAssemblage2 correctTable(
            "401,402,403,404/410,411,412,413/106,107,108/108,109,110/111,112,113/207,208,209/210,211,212/311,312,313/101,000,103,104"
    );
    //m1heS = "O2 K2 K10";

    //std::cout << "result:\n" << result << "\n";
    //std::cout << "should be:\n" << correctTable << "\n";

    // we should get at least 30, but it's ok if we have better result.
    //EXPECT_EQ(30, result.sizeTotal());
    EXPECT_LE(result.sizeTotal(),30) << "result:\n" << result << "\n" << "should be:\n" << correctTable << "\n";
}


TEST(testHeuristicSolver, testHeuristicSolverSolveJokerControversy) {
    HeuristicSolver hSolver;

    //We shouldn't be able to use this tile since we can't replace the joker with the right tile
    TilesCollection inputHand(
            "109"
    );

    TilesCollectionAssemblage2 inputTable(
            "101,201,301,000/105,106,107"
    );


    TilesCollectionAssemblage2 result = hSolver.Solve(inputTable, inputHand,1000);

    //std::cout << "result:\n" << result2 << "\n";
    EXPECT_EQ(7, result.sizeTotal());
}

TEST(testHeuristicSolver, testHeuristicSolverSolveNoResult) {
    HeuristicSolver hSolver;


    TilesCollection inputHand(
            "108"
    );

    TilesCollectionAssemblage2 inputTable(
            "301,302,303"
    );


    TilesCollectionAssemblage2 result = hSolver.Solve(inputTable, inputHand);

    //std::cout << "result:\n" << result2 << "\n";
    EXPECT_EQ(3, result.sizeTotal());
}


/**
 * This is not an actual test, it's a playground for sorting.
 * Comment out after it is not needed anymore.
 */
 /*
TEST(testRecursiveSolver, testSort1773tiles) {
    HeuristicSolver hSolver;

    //TODO stop using stupid hacks
    TilesCollectionAssemblage2 tca(

#include "../RummikubSolver/1773TilesCollections.inc"

    );

    tca.sort();

    std::cout << "result:\n" << tca << "\n";

}
*/

int main(int argc, char *argv[]) {
    /*The method is initializes the Google framework and must be called before RUN_ALL_TESTS */
    ::testing::InitGoogleTest(&argc, argv);

    /*RUN_ALL_TESTS automatically detects and runs all the tests defined using the TEST macro.
    It's must be called only once in the code because multiple calls lead to conflicts and,
    therefore, are not supported.
    */
    return RUN_ALL_TESTS();
}

