/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include <iostream>
#include <assert.h>
#include <iomanip>
#include "Tile.h"

Tile::Tile(unsigned char c, unsigned char num) {
    assert(num >= 0 && num <= 13 && c >= 0 && c <= 4);
    number = num;
    color = Color(c);
    joker = (c == 0 || num == 0);
};

Tile::Tile(unsigned char c, unsigned char num, bool isJoker) {
    assert(num >= 0 && num <= 13 && c >= 0 && c <= 4);
    number = num;
    color = Color(c);
    joker = isJoker;
};

Tile::Tile(Color c, unsigned char num) {
    assert((num > 0 && num <= 13 && c != NOCOLOR) || (num == 0 && c == NOCOLOR));
    number = num;
    color = c;
    joker = (c == 0 || num == 0);
};

Tile::Tile(Color c, unsigned char num, bool isJoker) {
    assert(num >= 0 && num <= 13);
    number = num;
    color = c;
    joker = isJoker;
};

unsigned char Tile::getNumber() const {
    return number;
}

Color Tile::getColor() const {
    return color;
}

bool Tile::isJoker() const {
    return joker;
}

bool Tile::isVirginJoker() const {
    return number == 0 && color == NOCOLOR && joker;
}

bool Tile::operator==(const Tile &other) const {
    return other.number == number && other.color == color;
}

bool Tile::operator!=(const Tile &other) const {
    return other.number != number || other.color != color;
}

//TODO output format should be configurable with command-line options
std::ostream &operator<<(std::ostream &os, const Tile &t) {
    //uses letter as color
    //os << std::setfill('0') << t.getColorLetter()  << std::setw(2) << ((int)t.number);
    //uses number as color
    //os << std::setfill('0') << t.color  << std::setw(2) << ((int)t.number);
    //uses number as color as well as colorize terminal output (vt100, not windows)
    //os << std::setfill('0') << t.getAnsiEscape() << t.color << std::setw(2) << ((int) t.number) << "\033[0m";
    // Always outputs joker without number
    os << std::setfill('0') << t.getAnsiEscape() << t.color << std::setw(2) << (((int) t.color) ? ((int) t.number) : 0) << "\033[0m";
    return os;
}

char Tile::getColorLetter() const {
    switch (color) {
        case 0:
            return 'J';//Joker
        case 1:
            return 'R';
        case 2:
            return 'C';//Cyan
        case 3:
            return 'Y';
        case 4:
            return 'B';
        default:
            return 'X';
    }
}

std::string Tile::getAnsiEscape() const {
    if (joker) {
        switch (color) {
            case 0:
                return "\033[7m\033[1;37m";//Joker
            case 1:
                return "\033[7m\033[1;31m";
            case 2:
                return "\033[7m\033[1;34m";
            case 3:
                return "\033[7m\033[1;33m";
            case 4:
                return "\033[7m\033[1;30m";
            default:
                return "\033[7m\033[1;32m";
        }
    } else {
        switch (color) {
            case 0:
                return "\033[1;37m";//Joker
            case 1:
                return "\033[1;31m";
            case 2:
                return "\033[1;34m";
            case 3:
                return "\033[1;33m";
            case 4:
                return "\033[1;30m";
            default:
                return "\033[1;32m";
        }
    }
}

bool Tile::operator<(const Tile &rhs) const {
    if (number == rhs.number) {
        return (int) color > (int) rhs.color;
    }
    return number < rhs.number;
}
