#MAKE_OPTIONS=-j 32

# Builds all the projects in the solution...
.PHONY: all_projects
all_projects: RummikubSolver RummikubSolverUnitTest

# Builds project 'RummikubSolver'...
.PHONY: RummikubSolver
RummikubSolver:
	$(MAKE) $(MAKE_OPTIONS) --directory="RummikubSolver/" --file=RummikubSolver.makefile

.PHONY: RummikubSolverDebug
RummikubSolverDebug:
	$(MAKE) $(MAKE_OPTIONS) --directory="RummikubSolver/" --file=RummikubSolver.makefile Debug

.PHONY: RummikubSolverRelease
RummikubSolverRelease:
	$(MAKE) $(MAKE_OPTIONS) --directory="RummikubSolver/" --file=RummikubSolver.makefile Release

# Builds project 'RummikubSolverUnitTest'...
.PHONY: RummikubSolverUnitTest
RummikubSolverUnitTest: RummikubSolver
#This is a stupid hack to ensure that the binary is always rebuilt
#Because Make cannot check if target has been updated in another makefile
#TODO put everything in a giant makefile?
	-rm RummikubSolverUnitTest/test__gccDebug/RummikubSolverUnitTest.o
	-rm RummikubSolverUnitTest/test__gccRelease/RummikubSolverUnitTest.o
	$(MAKE) $(MAKE_OPTIONS) --directory="RummikubSolverUnitTest/" --file=RummikubSolverUnitTest.makefile


.PHONY: RummikubSolverUnitTestDebug
RummikubSolverUnitTestDebug: RummikubSolverDebug
	-rm RummikubSolverUnitTest/test__gccDebug/RummikubSolverUnitTest.o
	$(MAKE) $(MAKE_OPTIONS) --directory="RummikubSolverUnitTest/" --file=RummikubSolverUnitTest.makefile Test__Debug

.PHONY: RummikubSolverUnitTestRelease
RummikubSolverUnitTestRelease: RummikubSolverRelease
	-rm RummikubSolverUnitTest/test__gccRelease/RummikubSolverUnitTest.o
	$(MAKE) $(MAKE_OPTIONS) --directory="RummikubSolverUnitTest/" --file=RummikubSolverUnitTest.makefile Test__Release

# Compiles and runs test
.PHONY: test
test: RummikubSolverUnitTestDebug
	./test__gccDebug/RummikubSolverUnitTest.exe

# Cleans all projects...
.PHONY: clean
clean:
	$(MAKE) --directory="RummikubSolver/" --file=RummikubSolver.makefile clean
	$(MAKE) --directory="RummikubSolverUnitTest/" --file=RummikubSolverUnitTest.makefile test__clean
