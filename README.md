# Rummikub Solver #

The board game [Rummikub][wikirummikub] requires the players to do a lot of repetitive thinking. 
The purpose of this application is to calculate what move(s) a player can take. 
As input, the application uses input files which describe a state of the game 
(tiles in players rack and tiles on the table).


## Repository/Code structure ##

* `/RummikubSolver` contains the main logic.
	* `Tile.*` A tile is a simple tile on the board. It has a color and a number.
	* `TilesCollection.*` A number of tiles, with no specific order.
	* `TilesCollectionAssemblage.*` A number of tilesCollections, with no specific order.
	
	* `ILPSolver.*` A solver algorithm based on Integer Linear Programming **(to be done)**[1].
	* `BasicSolver.*` A recursive guaranteed-best-result algorithm.
	* `HeuristicSolver.*` A solver algorithm based on heuristic guided state space search[2] .
	    * `Group.*` Contains logic for a TilesCollection of type Group. Inherits TilesCollection class.
	    * `Run.*` Contains logic for a TilesCollection of type Run. Inherits TilesCollection class.
	    * `Node.*` Contains the combination manipulation logic.
	
	* `IO.*`	For input/output, although some of this code has been moved into the object classes.
	
 
* `/RummikubSolverUnitTest` contains the unit tests.
 
* `/Testdata` contains input data for some of the tests.

## How to build ##

Requirements:

* c++11 compatible compiler
* libBoost
* googletest, Google C++ Testing Framework (for the unit tests)

The application should compile and pass the unit tests on the following systems:

OS      | Compiler
------- | -------------
Linux   | GCC 4.8.4
Linux   | GCC 4.9.0 (experimental, commit 6bca773)
Windows | Visual Studio 2010 (32-bit C/C++ Optimizing Compiler Version 16.00.40219.01 for 80x86)


`make test` compiles and runs the unit tests. Note that some tests can fail when run on a much slower
or faster computer than the original development computer. 

## Troubleshooting ##

* boost library is missing
  For ubuntu/debian machines, run `sudo apt install libboost-all-dev`

* gtest.h file not found
  Turns out google testing framework is installing in a weird path,
  For ubuntu/debian machines, making a symlink should fix the error
  `sudo ln -s /usr/src/googletest/googletest/include/gtest /usr/local/include/gtest`


[wikirummikub]: http://en.wikipedia.org/wiki/Rummikub

[1]: D. Den Hertog and P. B. Hulshof. Solving rummikub problems by integer linear programming. The Computer Journal, 49(6):665–669, Nov 2006.

[2]: David Samuelson and Michael Seibert. Solving rummikub with heuristic guided state space search. 2009.