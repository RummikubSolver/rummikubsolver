/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MYSTRINGS_H
#define MYSTRINGS_H

namespace MyStrings {
    const std::string exArgument0SizeRun = "Cannot make a 0 length Run!";
    const std::string exArgument0SizeGroup = "Cannot make a 0 length Group!";
    const std::string exArgumentSizeTotalRunF = "Cannot have a run of size %s starting on %s";
    const std::string exArgumentNullRun = "Run constructor cannot take nulls";
    const std::string exArgumentValueRunF = "Value %d is not a valid start value in Run";
    const std::string exArgumentSizeRunF = "Size %d is not a valid size in Run";
    const std::string exRemoveFromOneLengthRun = "Cannot remove a card from a one-length Run!";
    const std::string exRemoveFromMiddleRun = "Cannot remove a middle card from a Run!";
    const std::string exRemoveNonExistingCardF = "Card %s does not exist!";
    const std::string exAdd14 = "Cannot add to the end of 13!";
    const std::string exAddColor = "Wrong color!";
    const std::string exAddValue = "Invalid rank!";
    const std::string exAdd0 = "Cannot add before 1!";
    const std::string exWtfRunWithoutColor = "Found Run without a color!";

    const std::string warnSingletonJokerFirstFirst = "Warning: assigning singleton plain joker to first color, first value.";
};


#endif