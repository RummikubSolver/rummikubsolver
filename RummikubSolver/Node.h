/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NODE_H
#define NODE_H

#include "TilesCollectionAssemblage2.h"

class Node;
class HeuristicSolver;
typedef std::shared_ptr<Node> NodePointer;
struct LogicalState {
    TilesCollectionAssemblage2 board;
    TilesCollection hand;
};

class Node {

public:
    Node(Node *parent, TilesCollectionAssemblage2 unsorted, unsigned int depth, HeuristicSolver *parentSolver);
    bool operator<(const Node &rhs) const;
    bool operator>(const Node &rhs) const;
    bool operator==(const Node &other) const;
    bool operator!=(const Node &other) const;
    std::vector<NodePointer> getNeighbors();
    void handleSize1Group(unsigned int index, std::vector<NodePointer> *result);
    void handleRun(unsigned int index, std::vector<NodePointer> *result);
    void handleGroup(unsigned int index, std::vector<NodePointer> *result);
    bool isValid();
    bool hasParentSolver(); //for debugging
    LogicalState getState() const;
    NodePointer createNeighbor(TilesCollectionAssemblage2 groups);
    TilesCollectionAssemblage2 state; //move this here for now
    static std::size_t hash_value(Node const &n);
    friend std::ostream &operator<<(std::ostream &os, const Node &n);
    friend std::ostream &operator<<(std::ostream &os, const NodePointer &np);
    friend std::ostream &operator<<(std::ostream &os, const std::vector<std::shared_ptr<Node>> &tc);

protected:
    unsigned int depth;
    Node *prev;
    double heuristicVal;
    bool isLegal = true;
    std::vector<NodePointer> neighbours;
    HeuristicSolver *parentSolver;
};

#endif
