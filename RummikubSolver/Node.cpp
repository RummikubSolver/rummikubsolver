/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <sstream>
#include <algorithm>
#include <vector>
#include <iostream>
#include <memory>
#include "Tile.h"
#include "TilesCollection.h"
#include "TilesCollectionAssemblage2.h"
#include "HeuristicSolver.h"
#include "Run.h"
#include "Group.h"
#include "Node.h"

Node::Node(Node *parent, TilesCollectionAssemblage2 unsorted, unsigned int depth, HeuristicSolver *parentSolver) {
    this->prev = parent;
    this->state = unsorted;
    this->depth = depth;
    this->parentSolver = parentSolver;
    struct {
        inline bool operator()(std::shared_ptr<TilesCollection> &a, std::shared_ptr<TilesCollection> &b) const
        {
            return *a < *b;
        }
    } customLess2;
    std::sort(state.begin(), state.end(), customLess2);
    // debug
    unsigned int num = 0;

    TilesCollection handTemp = TilesCollection(parentSolver->getHand());

    for (unsigned int i = 0; i < state.size(); ++i) {
        num += state.at(i).size();
        if (!state.at(i).isLegal()) {
            TilesCollection cards = state.at(i);
            if (!isLegal || !handTemp.containsTiles(cards)) {
                isLegal = false;
            } else {
                handTemp.removeTiles(state.at(i));
            }
        }
    }

    //debug, amount of tiles in current state should match amount of tiles we started with
    if (num != parentSolver->getCardCount())
        std::cout << "bad logic!!!!!!\n";
}


bool Node::operator<(const Node &rhs) const {
    // This function counts number of invalid TileCollections
    int countThis = 0;
    for (auto it = state.cbegin(); it != state.cend(); it++) {
        if (!(*it)->isLegal()) {
            countThis++;
        }
    }

    int countThat = 0;
    for (auto it = rhs.state.cbegin(); it != rhs.state.cend(); it++) {
        if (!(*it)->isLegal()) {
            countThat++;
        }
    }

    return countThis < countThat;
}

bool Node::operator>(const Node &rhs) const {
    return rhs < *this;
}

bool Node::operator==(const Node &other) const {
    return Node::hash_value(*this) == Node::hash_value(other);
}

bool Node::operator!=(const Node &other) const {
    return state != other.state;
}

std::vector<NodePointer> Node::getNeighbors() {
    if (neighbours.size() == 0) {
        std::vector<NodePointer> result;
        unsigned int index = 0;
        for (auto it = state.begin(); it != state.end(); ++it) {
            //std::cout << "getNeighbors:loop start:state \n&&&\n" << state << "&&&\n";
            //std::cout << "getNeighbors start, handling: " << **it << "\n";
            if ((*it)->size() == 1) {
                // Put single cards in any group/run they fit in
                handleSize1Group(index, &result);
            } else if ((*it)->getType() == RUN) {
                // Remove a card from either end of a Run
                handleRun(index, &result);
            } else if ((*it)->getType() == GROUP) {
                // Remove a card from a group
                handleGroup(index, &result);
            } else {
                std::cerr << "getNeighbours(): not size 1, not a run or a group: " << (*it) << "\n";
            }
            //std::cout << "getNeighbors:loop end  :state \n&&&\n" << state << "&&&\n";
            index++;
        }

        neighbours.swap(result);

        // We need the internal collections sorted before sorting nodes
        // Needs custom sorting so we are not sorting by pointers
        struct {
            inline bool operator()(std::shared_ptr<TilesCollection> &a, std::shared_ptr<TilesCollection> &b) const
            {
                return *a < *b;
            }
        } customLess2;
        for (auto &node:neighbours) {
            std::sort(node->state.begin(), node->state.end(), customLess2);
        }

        // Needs custom sorting so we are not sorting by pointers
        struct {
            inline bool operator()(std::shared_ptr<Node> &a, std::shared_ptr<Node> &b) const
            {
                return *a < *b;
            }
        } customLess;
        std::sort(neighbours.begin(), neighbours.end(), customLess);
    }
    return neighbours;
}

void Node::handleSize1Group(unsigned int index, std::vector<NodePointer> *neighborNodes) {
    Tile c = state.at(index).at(0);
    bool isVirginJoker = state.at(index).isVirginJoker();
    TilesCollectionAssemblage2 stateCopy = state;

    //std::cout << "handleSize1Group start, handling: " << c << "\n";

    // Remove current
    stateCopy.removeOne(state.at(index));

    for (unsigned int i = 0; i < stateCopy.size(); i++) { //stateCopy is +1 ?
        if (stateCopy.at(i).size() == 1) {
            Tile other = stateCopy.at(i).at(0);
            //std::cout << "--Can we create group of tile " << c << " with tile " << other << " : ";
            if (isVirginJoker || other.isVirginJoker() ||
                (c.getNumber() == other.getNumber() && c.getColor() != other.getColor())) {
                TilesCollectionAssemblage2 temp(stateCopy);
                std::shared_ptr<Group> g = std::make_shared<Group>();
                g->add(c);
                g->add(other);
                temp.insertAtGroup(i,g);
                neighborNodes->push_back(createNeighbor(temp));
                //std::cout << "Yes! " << *g << "\n";
            } else {
                //std::cout << "No\n";
            }
        }

        // Add to either end of a run
        if (stateCopy.at(i).getType() == RUN) {
            std::shared_ptr<Run> r = std::make_shared<Run>(stateCopy.at(i));
            //std::cout << "--Can we add tile " << c << " to run " << *r << " : ";
            if (!isVirginJoker && r->getColor() != c.getColor()) {
                //std::cout << "No, was not a virginjoker or was wrong color\n";
                continue;
            }
            std::size_t startRank = r->getStartRank();
            std::size_t endRank = r->getEndRank();
            if (isVirginJoker) {
                if (startRank == 1 && endRank == 13) {
                    //std::cout << "No, this is a full run already\n";
                    continue;
                }
            }

            if ((isVirginJoker && endRank < 13) || startRank + r->size() == c.getNumber()) {
                //std::cout << "Yes, add to END";
                TilesCollectionAssemblage2 temp(stateCopy);
                temp.insertAtRun(i,r->addToEnd(c));
                //std::cout << ", looks like this: " << temp.at(i);
                neighborNodes->push_back(createNeighbor(temp));
            } else if ((isVirginJoker && startRank > 1 ) || startRank - 1 == c.getNumber()) {
                //std::cout << "Yes, add to BEGINNING";
                TilesCollectionAssemblage2 temp(stateCopy);
                temp.insertAtRun(i,r->addToStart(c));
                neighborNodes->push_back(createNeighbor(temp));
            }
            //std::cout << "\n";
        }
            // Add to a group
        else if (stateCopy.at(i).getType() == GROUP) {
            std::shared_ptr<Group> g = std::make_shared<Group>(stateCopy.at(i));
            //std::cout << "--Can we add tile " << c << " to grp " << *g << " : ";
            if (g->size() < 4) {
                auto nonExistingColors = g->getNonExistingColors();
                if (isVirginJoker || (g->getNumber() == c.getNumber() &&
                                        (std::find(nonExistingColors.begin(), nonExistingColors.end(),
                                                   c.getColor()) != nonExistingColors.end()))) {
                    TilesCollectionAssemblage2 temp(stateCopy);
                    g->add(c);
                    temp.insertAtGroup(i,g);
                    //std::cout << "Yes, add to group, looks like this: " << temp;
                    neighborNodes->push_back(createNeighbor(temp));
                }
            }
            //std::cout << "\n";
        }
    }
    //std::cout << "handleSize1Group end, handling: " << c << "\n";
}

void Node::handleRun(unsigned int index, std::vector<NodePointer> *result) {

    //std::cout << "handleRun() with index: " << index << "\n";
    //std::cout << "handleRun():state: \n###\n" << state<< "###\n";
    std::shared_ptr<Run> r = std::make_shared<Run>(state.at(index));

    TilesCollectionAssemblage2 stateCopy(state);
    TilesCollectionAssemblage2 stateCopy2(state);

    Tile first = r->at(0);
    Tile last = r->at(r->size() - 1);

    //std::cout << "handleRun(), handling: " << r << "\n";
    //std::cout << "first: " << first << "\n";
    //std::cout << "last: " << last << "\n";

    //there is no idea to try{} here, better to just crash... I guess :>
    //std::cout << "handleRun():1 we now have \n///\n" << stateCopy<< "///\n";
    //std::cout << "handleRun(): removing " << first << " from "<<  r << "\n";
    //stateCopy.at(index) = r->remove(first);
    //stateCopy.at(index) = *(std::make_shared<Run>(r->remove(first)));
    stateCopy.insertAtRun(index,r->remove(first));
    //std::cout << "handleRun(): inserting " << first<< " as new tile\n";
    stateCopy.push_back(Run(first));
    //std::cout << "handleRun():2 we now have \n///\n" << stateCopy<< "\n///\n";
    result->push_back(createNeighbor(stateCopy));

    if (state.at(index).size() > 2) {
        stateCopy2.insertAtRun(index, r->remove(last));
        stateCopy2.push_back(Run(last));
        result->push_back(createNeighbor(stateCopy2));
    }

}

void Node::handleGroup(unsigned int index, std::vector<NodePointer> *result) {
    std::shared_ptr<Group> g = std::make_shared<Group>(state.at(index));
    //std::cout << "In handleGroup, handling: " << g << "\n";
    if (g->size() == 2) {
        // Mmake a copy of state; replace state[index] with first tile, add last tile to state
        TilesCollectionAssemblage2 stateCopy = TilesCollectionAssemblage2(state);
        stateCopy.insertAtRun(index, g->at(0));
        stateCopy.push_back(Run(g->at(1)));
        result->push_back(createNeighbor(stateCopy));
    } else {
        for (auto c = g->begin(); c < g->end(); c++) {
            Group removed = g->remove(*c);
            if (c->isJoker()) {
                // Loop over all colors which are not currently in the group (not counting jokers)
                for (auto col : g->getNonExistingColors()) {
                    TilesCollectionAssemblage2 stateCopy = TilesCollectionAssemblage2(state);
                    std::shared_ptr<Group> tempG = std::make_shared<Group>(removed);
                    stateCopy.insertAtGroup(index, tempG);
                    stateCopy.push_back(Run(Tile(col, c->getNumber(), true)));
                    result->push_back(createNeighbor(stateCopy));
                }
            } else {
                TilesCollectionAssemblage2 stateCopy = TilesCollectionAssemblage2(state);
                std::shared_ptr<Group> tempG = std::make_shared<Group>(g->remove(*c));
                stateCopy.insertAtGroup(index, tempG);
                stateCopy.push_back(Run(*c));
                result->push_back(createNeighbor(stateCopy));
            }
        }
    }
}

bool Node::isValid() {
    return isLegal;
}

bool Node::hasParentSolver() {
    return parentSolver != nullptr;
}

LogicalState Node::getState() const {
    TilesCollectionAssemblage2 board;
    TilesCollection hand;
    for (auto it = state.cbegin(); it != state.cend(); ++it) {
        if ((*it)->isLegal()) {
            board.add(**it);
        } else {
            for (auto it2 = (*it)->cbegin(); it2 != (*it)->cend(); ++it2) {
                hand.add(*it2);
            }
        }
    }
    LogicalState ls = {board, hand};
    return ls;
}

NodePointer Node::createNeighbor(TilesCollectionAssemblage2 groups) {
    //std::cout << "in createNeighbor() handling these: \n---\n" << groups << "---\n";
    //std::cout << "hash: " << Node::hash_value(*this) << "\n";
    NodePointer temp = std::make_shared<Node>(this, groups, depth + 1, parentSolver);

    if (parentSolver->isInMap(temp)) {
        //std::cout << "createNeighbor(): returning from map\n";
        return parentSolver->getFromMap(temp);
    } else {
        //std::cout << "createNeighbor(): not found, inserting to map\n";
        parentSolver->insertToMap(temp);
        return parentSolver->getFromMap(temp);
    }
}

/**
 * Based on BKDR Hash Function
 * @param n
 * @return
 */
std::size_t Node::hash_value(Node const &n) {
    std::size_t seed = 31;
    std::size_t hash = 0;
    for (auto tileCollectionIt = n.state.cbegin(); tileCollectionIt != n.state.cend(); ++tileCollectionIt) {
        for (auto tileIt = (*tileCollectionIt)->cbegin(); tileIt != (*tileCollectionIt)->cend(); ++tileIt) {
            hash = (hash * seed) + (tileIt->getNumber()+((tileIt->getColor()+1)*13));
        }
        hash = (hash * seed);
    }
    return hash;
}

std::ostream &operator<<(std::ostream &os, const Node &n) {

    // Print on multiple lines
    //os << "\nBoard:\n" << n.getState().board << "Hand:\n" << n.getState().hand << "\n";
    // Print on single line
    //os << "\nBoard:" << n.getState().board << " Hand:" << n.getState().hand << "\n";
    // Don't print what tiles are on hand and what is on the table
    os << "\n" << n.state << "\n";
    return os;
}

std::ostream &operator<<(std::ostream &os, const NodePointer &np) {
    os << *np;
    return os;
}

std::ostream &operator<<(std::ostream &os, const std::vector<std::shared_ptr<Node>> &tc) {
    for (auto it = tc.cbegin(); it != tc.cend(); ++it) {
        // Normal output
        os << **it << "\n";
    }
    return os;
}



