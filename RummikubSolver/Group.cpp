/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <algorithm>
#include <vector>
#include <memory>
#include <assert.h>

#include "MyStrings.h"
#include "Tile.h"
#include "TilesCollection.h"
#include "TilesCollectionAssemblage2.h"

#include "Group.h"

namespace ms = MyStrings;


Group::Group() {};

Group::Group(const TilesCollection &input) {

    assert(input.canBeCastToGroup());
    tileArray = input.tileArray;
    unsigned char group_number = 0;
    for (auto t:tileArray) {
        if (t.getNumber() != 0) {
            group_number = t.getNumber();
            break;
        }
    }
    //group_number now contains the number this group is based on.
    if (group_number != 0) {
        for (unsigned char i = 0; i < tileArray.size(); i++) {
            if (tileArray.at(i).isVirginJoker()) {
                tileArray.at(i) = Tile(tileArray.at(i).getColor(), group_number, tileArray.at(i).isJoker());
            }
        }
    }
    std::sort(tileArray.begin(), tileArray.end());
};


TCType Group::getType() const {
    return GROUP;
}


bool Group::isLegal() const {
    return size() >= 3 && size() <= 4;
}


unsigned char Group::getNumber() {
    return tileArray.back().getNumber();
}

Group Group::remove(Tile t) {
    if (size() == 1)
        throw std::runtime_error(ms::exArgument0SizeGroup);
    if (!containsTile(t))
        throw std::runtime_error(ms::exRemoveNonExistingCardF);
    Group retval = Group(*this);
    retval.removeOne(t);
    return retval;

}


std::vector<Color> Group::getNonExistingColors() const {
    std::vector<Color> retval;
    retval.push_back(RED);
    retval.push_back(BLUE);
    retval.push_back(YELLOW);
    retval.push_back(BLACK);

    // NOTE: since it is a group we can't optimize by checking if all colors have been removed already
    for (auto it = tileArray.cbegin(); it != tileArray.cend(); it++) {
        retval.erase(std::remove(retval.begin(), retval.end(), (*it).getColor()), retval.end());
    }
    return retval;

}
	
