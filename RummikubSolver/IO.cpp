/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include "TilesCollection.h"
#include "IO.h"

IO::IO() {};


/**
 * Reads rack and table from file. All tiles on table are mixed into a TilesCollection
 *
 * @param filename
 * @param rack
 * @param table
 * @return an array with two elements, the (own) rack and the table as TilesCollection.
 */
void IO::readTilesFromFile(std::string filename, TilesCollection &rack, TilesCollection &table) {

    std::string tempStr;
    bool firstLineChecked = false;
    std::ifstream inFile;
    inFile.open(filename.c_str());
    if (!inFile.is_open()) {
        std::cerr << "ERROR: Could not open file " << filename << "\n";
    } else {
        while (getline(inFile, tempStr)) {
            boost::algorithm::trim(tempStr);
            //If first char is a #, skip it.
            if (!tempStr.empty() && tempStr[0] != '#') {
                TilesCollection tmpTA(tempStr);
                if (!firstLineChecked) {
                    rack = tmpTA;
                    firstLineChecked = true;
                } else {
                    table = tmpTA;
                }
            }
        }
        if (inFile.bad()) {
            std::cerr << "ERROR: File error " << filename << "\n";
        }
    }
    inFile.close();
}

/**
 * Reads rack and table from file. Tile collections state is preserved in an TilesCollectionAssemblage2.
 *
 * @param filename
 * @param rack
 * @param table
 * @return an array with two elements, the (own) rack and the table as TilesCollectionAssemblage2.
 */
void IO::readTilesFromFile(std::string filename, TilesCollection &rack, TilesCollectionAssemblage2 &table) {

    std::string tempStr;
    bool firstLineChecked = false;
    std::ifstream inFile;
    inFile.open(filename.c_str());
    if (!inFile.is_open()) {
        std::cerr << "ERROR: Could not open file " << filename << "\n";
    } else {
        while (getline(inFile, tempStr)) {
            boost::algorithm::trim(tempStr);
            // If string is empty or first char is a #, skip it.
            if (!tempStr.empty() && tempStr[0] != '#') {

                if (!firstLineChecked) {
                    TilesCollection tmpTA(tempStr);
                    rack = tmpTA;
                    firstLineChecked = true;
                } else {
                    TilesCollectionAssemblage2 tmpTCA2(tempStr);
                    table = tmpTCA2;
                }
            }
        }
        if (inFile.bad()) {
            std::cerr << "ERROR: File error " << filename << "\n";
        }
    }
    inFile.close();
}
