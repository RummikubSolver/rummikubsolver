/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <sstream>
#include <algorithm>
#include <memory>
#include <vector>
#include <iostream>
#include <assert.h>
#include "MyStrings.h"
#include "Tile.h"
#include "TilesCollection.h"
#include "TilesCollectionAssemblage2.h"
#include "Run.h"

namespace ms = MyStrings;

Run::Run() {
    //tileArray;
}

Run::Run(const TilesCollection &input) {
    TilesCollection copyTC = input;
    assert(copyTC.canBeCastToRun());
    tileArray = input.tileArray;
    std::sort(tileArray.begin(), tileArray.end());
};

Run::Run(const Tile &input) {
    this->add(input);
};

TCType Run::getType() const {
    return RUN;
}

bool Run::isLegal() const {
    return size() > 2;
}

Color Run::getColor() const {
    for (Tile t: tileArray) {
        if (t.getColor() != NOCOLOR) {
            return t.getColor();
        }
    }
    return NOCOLOR;
}

unsigned char Run::getStartRank() {
    for (Tile t: tileArray) {
        if (t.getNumber() != 0) {
            return t.getNumber();
        }
    }
    return 1;
}

unsigned char Run::getEndRank() {
    return tileArray.back().getNumber();
}

Run Run::remove(Tile t) {
    if (size() == 1) {
        throw std::runtime_error(ms::exArgument0SizeRun);
    }
    if (!containsTile(t)) {
        throw std::runtime_error(ms::exRemoveNonExistingCardF);
    }

    // Tiles can only be removed from the beginning or the end of a run
    // Jokers should have a number unless they were the first to be added, so accept virgin jokers as well
    if (t.getNumber() == getStartRank() || t.getNumber() == getEndRank() || t.isVirginJoker()) {
        Run tempRun(*this);
        tempRun.removeOne(t);
        return tempRun;
    } else {
        throw std::runtime_error("REMOVED FROM MIDDLE");
    }
}

Run Run::addToStart(Tile c) {
    if (this->getStartRank() == 1) {
        throw std::runtime_error(ms::exAdd0);
    }
    if (c.isJoker()) {
        Run result(*this);
        result.add(Tile(this->getColor(), this->getStartRank() - 1, true));
        return result;
    }
    if (c.getColor() != this->getColor()) {
        throw std::runtime_error(ms::exAddColor);
    }
    if (c.getNumber() != (this->getStartRank() + -1)) {
        throw std::runtime_error(ms::exAddValue);
    }
    Run result(*this);
    result.add(c);
    return result;
}

Run Run::addToEnd(Tile c) {
    if (size() + this->getStartRank() == 14) {
        throw std::runtime_error(ms::exAdd14);
    }
    if (c.isJoker()) {
        Run result(*this);
        result.add(Tile(this->getColor(), this->getEndRank() + 1, true));
        return result;

    }
    if (c.getColor() != this->getColor()) {
        std::cout << "trying to add tile " << c << " to run " << *this << "\n";
        throw std::runtime_error(ms::exAddColor);
    }
    if (c.getNumber() != (this->getEndRank() + 1)) {
        std::cout << "trying to add tile " << c << " to run " << *this << "\n";
        throw std::runtime_error(ms::exAddValue);
    }
    Run result(*this);
    result.add(c);
    return result;
}
