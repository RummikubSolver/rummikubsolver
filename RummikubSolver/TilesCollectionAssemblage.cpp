/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stdafx.h"
#include "crossplatform.h"
#include <vector>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include "Tile.h"
#include "TilesCollection.h"
#include "Run.h"
#include "Group.h"
#include "TilesCollectionAssemblage.h"

TilesCollectionAssemblage::TilesCollectionAssemblage() : tcArray() {};

TilesCollectionAssemblage::TilesCollectionAssemblage(std::string input) {
    this->add(input);
}

// This function should only return Groups and Runs, and choke on other input
TilesCollectionAssemblage::TilesCollectionAssemblage(std::string input, bool makeSpecialTC) {
    if (makeSpecialTC) {
        this->addSpecial(input);
    }
}

TilesCollectionAssemblage::TilesCollectionAssemblage(const TilesCollectionAssemblage &input) {
    tcArray = input.tcArray;
};

void TilesCollectionAssemblage::add(TilesCollection tc) {
    tcArray.push_back(tc);
};

// This one adds TileCollections (no validation whatsoever)
void TilesCollectionAssemblage::add(std::string input) {

    std::vector<std::string> tempStrVec;
    // Split string into vector
    boost::algorithm::split(tempStrVec, input, boost::algorithm::is_any_of("/"));
    for (std::vector<std::string>::iterator it = tempStrVec.begin(); it != tempStrVec.end(); ++it) {
        TilesCollection tmpTC((*it));
        tcArray.push_back(tmpTC);
    }

}

// This one adds only valid Runs and Groups
void TilesCollectionAssemblage::addSpecial(std::string input) {
    std::vector<std::string> tempStrVec;
    boost::algorithm::split(tempStrVec, input, boost::algorithm::is_any_of("/"));
    for (std::vector<std::string>::iterator it = tempStrVec.begin(); it != tempStrVec.end(); ++it) {
        TilesCollection tmpTC((*it));
        if (tmpTC.canBeCastToRun()) {
            Run tmpTC((*it));
        } else if (tmpTC.canBeCastToGroup()) {
            Group tmpTC((*it));
        } else {
            // Not a Run or a Group!!!
            std::cerr << "this is not a run or a group!: " << (*it) << "\n";
            assert(false);
        }
        tcArray.push_back(tmpTC);
    }
}

void TilesCollectionAssemblage::addTileToEach(Tile t) {
    for (std::vector<TilesCollection>::iterator it = tcArray.begin(); it != tcArray.end(); ++it) {
        (*it).add(t);
    }
}

void TilesCollectionAssemblage::addTileToEven(Tile t) {
    for (unsigned int i = 0; i < tcArray.size(); i++) {
        if (i % 2 == 0) {
            tcArray[i].add(t);
        }
    }
}

void TilesCollectionAssemblage::addTileToOdd(Tile t) {
    for (unsigned int i = 0; i < tcArray.size(); i++) {
        if (i % 2 == 1) {
            tcArray[i].add(t);
        }
    }
}

void TilesCollectionAssemblage::removeOne(TilesCollection tc) {
    tcIterator = std::find(tcArray.begin(), tcArray.end(), tc);
    if (tcIterator != tcArray.end()) {
        tcArray.erase(tcIterator);
    }
}

/**
 * @return amount of TileCollections in object
 */
unsigned int TilesCollectionAssemblage::size() const NOEXCEPT {
    return tcArray.size();
}

/**
 * @return amount of Tiles in object
 */
unsigned int TilesCollectionAssemblage::sizeTotal() const NOEXCEPT {
    unsigned int result = 0;
    for (auto tc:tcArray) {
        result += tc.size();
    }
    return result;
}

bool TilesCollectionAssemblage::containsTile(Tile t) const {
    for (std::vector<TilesCollection>::const_iterator it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if ((*it).containsTile(t)) {
            return true;
        }
    }
    return false;
}

unsigned int TilesCollectionAssemblage::countTile(Tile t) const {
    unsigned int count = 0;
    for (std::vector<TilesCollection>::const_iterator it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if ((*it).containsTile(t)) {
            count++;
        }
    }
    return count;
}

unsigned int TilesCollectionAssemblage::countTilesCollection(TilesCollection tc) const {
    unsigned int count = 0;
    for (std::vector<TilesCollection>::const_iterator it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if ((*it) == tc) {
            count++;
        }
    }
    return count;
}

TilesCollectionAssemblage TilesCollectionAssemblage::getDuplicates() const {
    TilesCollectionAssemblage result;
    for (std::vector<TilesCollection>::const_iterator it = tcArray.cbegin(); it != tcArray.cend(); ++it) {
        if (this->countTilesCollection(*it) > 1 && !result.countTilesCollection(*it)) {
            result.add(*it);
        }
    }
    return result;
}

void TilesCollectionAssemblage::sort() {
    //sort every TilesCollection
    for (std::vector<TilesCollection>::iterator it = tcArray.begin(); it != tcArray.end(); ++it) {
        std::sort((*it).begin(), (*it).end());
    }

    //sort "outside"
    std::sort(tcArray.begin(), tcArray.end());
}

void TilesCollectionAssemblage::duplicate() {
    std::vector<TilesCollection> tcArrayCopy(tcArray);
    if (tcArray.size() > 0) {
        tcArray.reserve(tcArray.size() + 1);
        for (std::vector<TilesCollection>::iterator it = tcArrayCopy.begin(); it != tcArrayCopy.end(); ++it) {
            tcArray.push_back((*it));
        }
    }
}

bool TilesCollectionAssemblage::equals(TilesCollectionAssemblage &other) {
    (*this).sort();
    other.sort();
    return (tcArray == other.tcArray);
}

std::ostream &operator<<(std::ostream &os, const TilesCollectionAssemblage &tc) {
    for (std::vector<TilesCollection>::const_iterator it = tc.cbegin(); it != tc.cend(); ++it) {
        os << *it << "\n";
    }
    return os;
}

bool TilesCollectionAssemblage::operator==(const TilesCollectionAssemblage &other) const {
    if (tcArray.size() != other.tcArray.size()) {
        return false;
    } else {
        std::vector<TilesCollection> thisCopy(this->tcArray);
        std::vector<TilesCollection> otherCopy(other.tcArray);

        std::sort(thisCopy.begin(), thisCopy.end());
        std::sort(otherCopy.begin(), otherCopy.end());

        for (unsigned int i = 0; i < thisCopy.size(); i++) {
            if (thisCopy[i] != otherCopy[i]) {
                return false;
            }
        }
    }
    return true;
}


bool TilesCollectionAssemblage::operator!=(const TilesCollectionAssemblage &other) const {
    return !((*this) == other);
}

TilesCollection TilesCollectionAssemblage::at(std::vector<TilesCollection>::size_type n) {
    return tcArray.at(n);
}

std::vector<TilesCollection, std::allocator<TilesCollection> >::iterator TilesCollectionAssemblage::begin() {
    return tcArray.begin();
}

std::vector<TilesCollection, std::allocator<TilesCollection> >::iterator TilesCollectionAssemblage::end() {
    return tcArray.end();
}

std::vector<TilesCollection, std::allocator<TilesCollection> >::const_iterator
TilesCollectionAssemblage::cbegin() const {
    return tcArray.cbegin();
}

std::vector<TilesCollection, std::allocator<TilesCollection> >::const_iterator TilesCollectionAssemblage::cend() const {
    return tcArray.cend();
}

void TilesCollectionAssemblage::push_back(const TilesCollection &val) {
    tcArray.push_back(val);
}

void TilesCollectionAssemblage::push_back(TilesCollection &&val) {
    tcArray.push_back(val);
}

void TilesCollectionAssemblage::reserve(std::vector<TilesCollection>::size_type n) {
    tcArray.reserve(n);
}