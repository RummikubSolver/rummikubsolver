# Compiler flags...
CPP_COMPILER = g++
C_COMPILER = gcc

# Library paths...
Debug_Library_Path=
Release_Library_Path=

# Additional libraries...
#Debug_Libraries=
#Release_Libraries=
Debug_Libraries=-Wl,--start-group -lboost_program_options -pthread   -Wl,--end-group
Release_Libraries=-Wl,--start-group -lboost_program_options -pthread   -Wl,--end-group

# Preprocessor definitions...
Debug_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE
Release_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE 

# Implictly linked object files...
Debug_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=

# Compiler flags...
Debug_Compiler_Flags= -Wall -O0 -g -std=c++11
Release_Compiler_Flags= -Wall -O2 -std=c++11

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug Release 

#
# DEBUG
#

# Builds the Debug configuration...
.PHONY: Debug
Debug: create_folders gccDebug/IO.o gccDebug/RummikubSolver.o gccDebug/stdafx.o gccDebug/Tile.o gccDebug/TilesCollection.o gccDebug/TilesCollectionAssemblage.o gccDebug/TilesCollectionAssemblage2.o gccDebug/MyStrings.o gccDebug/Group.o gccDebug/Run.o gccDebug/Node.o gccDebug/Solver.o gccDebug/BasicSolver.o gccDebug/ILPSolver.o gccDebug/HeuristicSolver.o 
	$(CPP_COMPILER) gccDebug/IO.o gccDebug/RummikubSolver.o gccDebug/stdafx.o gccDebug/Tile.o gccDebug/TilesCollection.o  gccDebug/TilesCollectionAssemblage.o gccDebug/TilesCollectionAssemblage2.o gccDebug/MyStrings.o gccDebug/Group.o gccDebug/Run.o gccDebug/Node.o gccDebug/Solver.o gccDebug/BasicSolver.o gccDebug/ILPSolver.o gccDebug/HeuristicSolver.o $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,./ -o ../gccDebug/RummikubSolver.exe

# Compiles file IO.cpp for the Debug configuration...
-include gccDebug/IO.d
gccDebug/IO.o: IO.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c IO.cpp $(Debug_Include_Path) -o gccDebug/IO.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM IO.cpp $(Debug_Include_Path) > gccDebug/IO.d

# Compiles file RummikubSolver.cpp for the Debug configuration...
-include gccDebug/RummikubSolver.d
gccDebug/RummikubSolver.o: RummikubSolver.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c RummikubSolver.cpp $(Debug_Include_Path) -o gccDebug/RummikubSolver.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM RummikubSolver.cpp $(Debug_Include_Path) > gccDebug/RummikubSolver.d

# Compiles file stdafx.cpp for the Debug configuration...
-include gccDebug/stdafx.d
gccDebug/stdafx.o: stdafx.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c stdafx.cpp $(Debug_Include_Path) -o gccDebug/stdafx.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM stdafx.cpp $(Debug_Include_Path) > gccDebug/stdafx.d

# Compiles file Tile.cpp for the Debug configuration...
-include gccDebug/Tile.d
gccDebug/Tile.o: Tile.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c Tile.cpp $(Debug_Include_Path) -o gccDebug/Tile.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM Tile.cpp $(Debug_Include_Path) > gccDebug/Tile.d

# Compiles file TilesCollection.cpp for the Debug configuration...
-include gccDebug/TilesCollection.d
gccDebug/TilesCollection.o: TilesCollection.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c TilesCollection.cpp $(Debug_Include_Path) -o gccDebug/TilesCollection.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM TilesCollection.cpp $(Debug_Include_Path) > gccDebug/TilesCollection.d
	
# Compiles file TilesCollectionAssemblage.cpp for the Debug configuration...
-include gccDebug/TilesCollectionAssemblage.d
gccDebug/TilesCollectionAssemblage.o: TilesCollectionAssemblage.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c TilesCollectionAssemblage.cpp $(Debug_Include_Path) -o gccDebug/TilesCollectionAssemblage.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM TilesCollectionAssemblage.cpp $(Debug_Include_Path) > gccDebug/TilesCollectionAssemblage.d	
	
# Compiles file TilesCollectionAssemblage2.cpp for the Debug configuration...
-include gccDebug/TilesCollectionAssemblage2.d
gccDebug/TilesCollectionAssemblage2.o: TilesCollectionAssemblage2.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c TilesCollectionAssemblage2.cpp $(Debug_Include_Path) -o gccDebug/TilesCollectionAssemblage2.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM TilesCollectionAssemblage2.cpp $(Debug_Include_Path) > gccDebug/TilesCollectionAssemblage2.d
	
# Compiles file Solver.cpp for the Debug configuration...
-include gccDebug/Solver.d
gccDebug/Solver.o: Solver.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c Solver.cpp $(Debug_Include_Path) -o gccDebug/Solver.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM Solver.cpp $(Debug_Include_Path) > gccDebug/Solver.d
	
# Compiles file BasicSolver.cpp for the Debug configuration...
-include gccDebug/BasicSolver.d
gccDebug/BasicSolver.o: BasicSolver.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c BasicSolver.cpp $(Debug_Include_Path) -o gccDebug/BasicSolver.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM BasicSolver.cpp $(Debug_Include_Path) > gccDebug/BasicSolver.d

# Compiles file ILPSolver.cpp for the Debug configuration...
-include gccDebug/ILPSolver.d
gccDebug/ILPSolver.o: ILPSolver.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ILPSolver.cpp $(Debug_Include_Path) -o gccDebug/ILPSolver.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ILPSolver.cpp $(Debug_Include_Path) > gccDebug/ILPSolver.d

# Compiles file HeuristicSolver.cpp for the Debug configuration...
-include gccDebug/HeuristicSolver.d
gccDebug/HeuristicSolver.o: HeuristicSolver.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c HeuristicSolver.cpp $(Debug_Include_Path) -o gccDebug/HeuristicSolver.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM HeuristicSolver.cpp $(Debug_Include_Path) > gccDebug/HeuristicSolver.d

# Compiles file MyStrings.cpp for the Debug configuration...
-include gccDebug/MyStrings.d
gccDebug/MyStrings.o: MyStrings.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c MyStrings.cpp $(Debug_Include_Path) -o gccDebug/MyStrings.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM MyStrings.cpp $(Debug_Include_Path) > gccDebug/MyStrings.d

# Compiles file Group.cpp for the Debug configuration...
-include gccDebug/Group.d
gccDebug/Group.o: Group.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c Group.cpp $(Debug_Include_Path) -o gccDebug/Group.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM Group.cpp $(Debug_Include_Path) > gccDebug/Group.d
	
# Compiles file Run.cpp for the Debug configuration...
-include gccDebug/Run.d
gccDebug/Run.o: Run.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c Run.cpp $(Debug_Include_Path) -o gccDebug/Run.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM Run.cpp $(Debug_Include_Path) > gccDebug/Run.d
	
# Compiles file Node.cpp for the Debug configuration...
-include gccDebug/Node.d
gccDebug/Node.o: Node.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c Node.cpp $(Debug_Include_Path) -o gccDebug/Node.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM Node.cpp $(Debug_Include_Path) > gccDebug/Node.d
#
# RELEASE
#

# Builds the Release configuration...
.PHONY: Release
Release: create_folders gccRelease/IO.o gccRelease/RummikubSolver.o gccRelease/stdafx.o gccRelease/Tile.o gccRelease/TilesCollection.o gccRelease/TilesCollectionAssemblage.o gccRelease/TilesCollectionAssemblage2.o gccRelease/MyStrings.o gccRelease/Group.o gccRelease/Run.o gccRelease/Node.o gccRelease/Solver.o gccRelease/BasicSolver.o gccRelease/ILPSolver.o gccRelease/HeuristicSolver.o
	$(CPP_COMPILER) gccRelease/IO.o gccRelease/RummikubSolver.o gccRelease/stdafx.o gccRelease/Tile.o gccRelease/TilesCollection.o  gccRelease/TilesCollectionAssemblage.o gccRelease/TilesCollectionAssemblage2.o gccRelease/MyStrings.o gccRelease/Group.o gccRelease/Run.o gccRelease/Node.o gccRelease/Solver.o gccRelease/BasicSolver.o gccRelease/ILPSolver.o gccRelease/HeuristicSolver.o gccRelease/MyStrings.o $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,./ -o ../gccRelease/RummikubSolver.exe

# Compiles file IO.cpp for the Release configuration...
-include gccRelease/IO.d
gccRelease/IO.o: IO.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c IO.cpp $(Release_Include_Path) -o gccRelease/IO.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM IO.cpp $(Release_Include_Path) > gccRelease/IO.d

# Compiles file RummikubSolver.cpp for the Release configuration...
-include gccRelease/RummikubSolver.d
gccRelease/RummikubSolver.o: RummikubSolver.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c RummikubSolver.cpp $(Release_Include_Path) -o gccRelease/RummikubSolver.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM RummikubSolver.cpp $(Release_Include_Path) > gccRelease/RummikubSolver.d

# Compiles file stdafx.cpp for the Release configuration...
-include gccRelease/stdafx.d
gccRelease/stdafx.o: stdafx.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c stdafx.cpp $(Release_Include_Path) -o gccRelease/stdafx.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM stdafx.cpp $(Release_Include_Path) > gccRelease/stdafx.d

# Compiles file Tile.cpp for the Release configuration...
-include gccRelease/Tile.d
gccRelease/Tile.o: Tile.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c Tile.cpp $(Release_Include_Path) -o gccRelease/Tile.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM Tile.cpp $(Release_Include_Path) > gccRelease/Tile.d

# Compiles file TilesCollection.cpp for the Release configuration...
-include gccRelease/TilesCollection.d
gccRelease/TilesCollection.o: TilesCollection.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c TilesCollection.cpp $(Release_Include_Path) -o gccRelease/TilesCollection.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM TilesCollection.cpp $(Release_Include_Path) > gccRelease/TilesCollection.d
	
# Compiles file TilesCollectionAssemblage.cpp for the Release configuration...
-include gccRelease/TilesCollectionAssemblage.d
gccRelease/TilesCollectionAssemblage.o: TilesCollectionAssemblage.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c TilesCollectionAssemblage.cpp $(Release_Include_Path) -o gccRelease/TilesCollectionAssemblage.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM TilesCollectionAssemblage.cpp $(Release_Include_Path) > gccRelease/TilesCollectionAssemblage.d
	
# Compiles file TilesCollectionAssemblage2.cpp for the Release configuration...
-include gccRelease/TilesCollectionAssemblage2.d
gccRelease/TilesCollectionAssemblage2.o: TilesCollectionAssemblage2.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c TilesCollectionAssemblage2.cpp $(Release_Include_Path) -o gccRelease/TilesCollectionAssemblage2.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM TilesCollectionAssemblage2.cpp $(Release_Include_Path) > gccRelease/TilesCollectionAssemblage2.d
	
# Compiles file Solver.cpp for the Release configuration...
-include gccRelease/Solver.d
gccRelease/Solver.o: Solver.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c Solver.cpp $(Release_Include_Path) -o gccRelease/Solver.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM Solver.cpp $(Release_Include_Path) > gccRelease/Solver.d
	
# Compiles file BasicSolver.cpp for the Release configuration...
-include gccRelease/BasicSolver.d
gccRelease/BasicSolver.o: BasicSolver.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c BasicSolver.cpp $(Release_Include_Path) -o gccRelease/BasicSolver.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM BasicSolver.cpp $(Release_Include_Path) > gccRelease/BasicSolver.d
	
# Compiles file ILPSolver.cpp for the Release configuration...
-include gccRelease/ILPSolver.d
gccRelease/ILPSolver.o: ILPSolver.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ILPSolver.cpp $(Release_Include_Path) -o gccRelease/ILPSolver.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ILPSolver.cpp $(Release_Include_Path) > gccRelease/ILPSolver.d
	
# Compiles file HeuristicSolver.cpp for the Release configuration...
-include gccRelease/HeuristicSolver.d
gccRelease/HeuristicSolver.o: HeuristicSolver.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c HeuristicSolver.cpp $(Release_Include_Path) -o gccRelease/HeuristicSolver.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM HeuristicSolver.cpp $(Release_Include_Path) > gccRelease/HeuristicSolver.d

# Compiles file MyStrings.cpp for the Release configuration...
-include gccRelease/MyStrings.d
gccRelease/MyStrings.o: MyStrings.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c MyStrings.cpp $(Release_Include_Path) -o gccRelease/MyStrings.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM MyStrings.cpp $(Release_Include_Path) > gccRelease/MyStrings.d

# Compiles file Group.cpp for the Release configuration...
-include gccRelease/Group.d
gccRelease/Group.o: Group.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c Group.cpp $(Release_Include_Path) -o gccRelease/Group.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM Group.cpp $(Release_Include_Path) > gccRelease/Group.d	
	
# Compiles file Run.cpp for the Release configuration...
-include gccRelease/Run.d
gccRelease/Run.o: Run.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c Run.cpp $(Release_Include_Path) -o gccRelease/Run.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM Run.cpp $(Release_Include_Path) > gccRelease/Run.d

# Compiles file Node.cpp for the Release configuration...
-include gccRelease/Node.d
gccRelease/Node.o: Node.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c Node.cpp $(Release_Include_Path) -o gccRelease/Node.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM Node.cpp $(Release_Include_Path) > gccRelease/Node.d
#
# OTHER
#

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p gccDebug/source
	mkdir -p ../gccDebug
	mkdir -p gccRelease/source
	mkdir -p ../gccRelease

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -f gccDebug/*.o
	rm -f gccDebug/*.d
	rm -f ../gccDebug/*.a
	rm -f ../gccDebug/*.so
	rm -f ../gccDebug/*.dll
	rm -f ../gccDebug/*.exe
	rm -f gccRelease/*.o
	rm -f gccRelease/*.d
	rm -f ../gccRelease/*.a
	rm -f ../gccRelease/*.so
	rm -f ../gccRelease/*.dll
	rm -f ../gccRelease/*.exe
	rm -f *.h.gch

