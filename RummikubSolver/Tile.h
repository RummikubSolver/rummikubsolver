/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TILE_H
#define TILE_H

enum Color {
    RED = 1, BLUE = 2, YELLOW = 3, BLACK = 4, NOCOLOR = 0,
};

class Tile {
public:
    Tile(unsigned char c, unsigned char num);
    Tile(unsigned char c, unsigned char num, bool isJoker);
    Tile(Color c, unsigned char num);
    Tile(Color c, unsigned char num, bool isJoker);
    unsigned char getNumber() const;
    Color getColor() const;
    bool isJoker() const;
    bool isVirginJoker() const;
    bool operator<(const Tile &rhs) const;
    bool operator==(const Tile &other) const;
    bool operator!=(const Tile &other) const;
    friend std::ostream &operator<<(std::ostream &os, const Tile &t);

protected:
    Color color;
    unsigned char number;
    bool joker;
    char getColorLetter() const;
    std::string getAnsiEscape() const;
};
#endif
